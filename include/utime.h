#ifndef SYSDARFT_UTIME_H_
#define SYSDARFT_UTIME_H_

#include <sys/types.h>    /* I know - shouldn't do this, but .. */

struct utimbuf {
    time_t actime;
    time_t modtime;
};

extern int utime(const char *, struct utimbuf *);

#endif // SYSDARFT_UTIME_H_
