#ifndef SYSDARFT_TIME_H_
#define SYSDARFT_TIME_H_

#ifndef _TIME_T
# define _TIME_T
typedef long time_t;
#endif // _TIME_T

#ifndef _SIZE_T
# define _SIZE_T
typedef unsigned int size_t;
#endif // _SIZE_T

#define CLOCKS_PER_SEC 100

typedef long clock_t;

struct tm {
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday;
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
    int tm_isdst;
};

clock_t     clock       (void);
time_t      time        (time_t *);
double      difftime    (time_t, time_t);
time_t      mktime      (struct tm *);
char *      asctime     (const struct tm *);
char *      ctime       (const time_t *);
struct tm * gmtime      (const time_t *);
struct tm * localtime   (const time_t *);
size_t      strftime    (char *, size_t, const char *, const struct tm *);
void        tzset       (void);

#endif // SYSDARFT_TIME_H_
