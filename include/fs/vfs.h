#ifndef SYSDARFT_VFS_H
#define SYSDARFT_VFS_H

#include <kernel/kernel.h>

#define VFS_MAX_SIZE (64 * 1024) /* 64Kb */
#define VFS_ENTRY "vfs"

int vfs_mount(char * __user, char * __user dir_name, int rw_flag);

#endif //SYSDARFT_VFS_H
