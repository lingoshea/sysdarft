#ifndef SYSDARFT_CHAR_DEV_H_
#define SYSDARFT_CHAR_DEV_H_

#include <sys/types.h>
#include <kernel/fs.h>

ssize_t rw_char     (int, dev_t, char *, size_t, off_t *);
ssize_t read_pipe   (struct m_inode *, char *, size_t);
ssize_t write_pipe  (struct m_inode *, char *, size_t);
ssize_t file_read   (struct m_inode *, struct file *, char *, size_t, int);
ssize_t file_write  (struct m_inode *, struct file *, char *, size_t);

#endif // SYSDARFT_CHAR_DEV_H_
