#ifndef SYSDARFT_BLOCK_DEV_H_
#define SYSDARFT_BLOCK_DEV_H_

ssize_t block_write (dev_t, unsigned long *, char *, size_t);
ssize_t block_read  (dev_t, unsigned long *, char *, size_t);

#endif // SYSDARFT_BLOCK_DEV_H_
