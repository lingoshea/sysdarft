#ifndef SYSDARFT_MFS_H
#define SYSDARFT_MFS_H

#include <kernel/kernel.h>

int mfs_sync(void);
int mfs_close(unsigned int);
int mfs_dup2(fd_t oldfd, fd_t newfd);
int mfs_dup(fd_t fildes);
int mfs_fcntl(fd_t fd, unsigned int cmd, unsigned long arg);
int mfs_ioctl(fd_t fd, unsigned int cmd, unsigned long arg);
int mfs_mknod(const char *filename, int mode, dev_t dev);
int mfs_mkdir(const char *pathname, int mode);
int mfs_rmdir(const char *name);
int mfs_unlink(const char *name);
int mfs_link(const char *oldname, const char *newname);
int mfs_ustat(int dev, struct ustat *ubuf);
int mfs_utime(char *filename, struct utimbuf *times);
int mfs_access(const char *filename, int mode);
int mfs_chdir(const char *filename);
int mfs_chroot(const char *filename);
int mfs_chmod(const char *filename, int mode);
int mfs_chown(const char *filename, int uid, int gid);
int mfs_open(const char *filename, int flag, int mode);
int mfs_creat(const char *pathname, int mode);
int mfs_pipe(unsigned long *fildes);
int mfs_lseek(unsigned int fd, off_t offset, int origin);
ssize_t mfs_read(fd_t fd, char * __user buf, size_t count, int flags);
ssize_t mfs_write(fd_t fd, char *buf, size_t count);
ssize_t mfs_stat(char *filename, struct stat *statbuf);
ssize_t mfs_fstat(fd_t fd, struct stat *statbuf);
int mfs_umount(char * __user dev_name);
int mfs_mount(char * __user dev_name, char * __user dir_name, int rw_flag);

#endif //SYSDARFT_MFS_H
