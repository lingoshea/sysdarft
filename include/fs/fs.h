#ifndef SYSDARFT_FS_H
#define SYSDARFT_FS_H

#include <errno.h>
#include <fs/fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <utime.h>

#include <asm/segment.h>
#include <kernel/kernel.h>
#include <kernel/sched.h>
#include <kernel/tty.h>
#include <fs/fcntl.h>

// General filesystem APIs

typedef int (*sys_ustat_t)(int, struct ustat *);
typedef int (*sys_utime_t)(char *, struct utimbuf *);
typedef int (*sys_access_t)(const char *, int);
typedef int (*sys_chdir_t)(const char *);
typedef int (*sys_chroot_t)(const char *);
typedef int (*sys_chmod_t)(const char *, int);
typedef int (*sys_chown_t)(const char *, int, int);
typedef int (*sys_open_t)(const char *, int, int);
typedef int (*sys_creat_t)(const char *, int);
typedef int (*sys_close_t)(unsigned int);
typedef int (*sys_mknod_t)(const char *, int, dev_t);
typedef int (*sys_mkdir_t)(const char *, int);
typedef int (*sys_rmdir_t)(const char *);
typedef int (*sys_unlink_t)(const char *);
typedef int (*sys_link_t)(const char *, const char *);
typedef int (*sys_ioctl_t)(fd_t, unsigned int, unsigned long);
typedef int (*sys_pipe_t)(unsigned long *);
typedef int (*sys_lseek_t)(unsigned int, off_t, int);
typedef ssize_t (*sys_read_t)(fd_t, char * __user, size_t, int);
typedef ssize_t (*sys_write_t)(fd_t, char *, size_t);
typedef ssize_t (*sys_stat_t)(char *, struct stat *);
typedef ssize_t (*sys_fstat_t)(fd_t, struct stat *);
typedef int (*sys_umount_t)(char * __user);
typedef int (*sys_mount_t)(char * __user, char * __user, int);
typedef int (*sys_sync_t)(void);
typedef int (*sys_dup2_t)(fd_t, fd_t);
typedef int (*sys_dup_t)(fd_t);
typedef int (*sys_fcntl_t)(fd_t, unsigned int, unsigned long);

struct fs_ops_t
{
    sys_ustat_t sys_ustat;
    sys_utime_t sys_utime;
    sys_access_t sys_access;
    sys_chdir_t sys_chdir;
    sys_chroot_t sys_chroot;
    sys_chmod_t sys_chmod;
    sys_chown_t sys_chown;
    sys_open_t sys_open;
    sys_creat_t sys_creat;
    sys_close_t sys_close;
    sys_mknod_t sys_mknod;
    sys_mkdir_t sys_mkdir;
    sys_rmdir_t sys_rmdir;
    sys_unlink_t sys_unlink;
    sys_link_t sys_link;
    sys_ioctl_t sys_ioctl;
    sys_pipe_t sys_pipe;
    sys_lseek_t sys_lseek;
    sys_read_t sys_read;
    sys_write_t sys_write;
    sys_stat_t sys_stat;
    sys_fstat_t sys_fstat;
    sys_umount_t sys_umount;
    sys_mount_t sys_mount;
    sys_sync_t sys_sync;
    sys_dup2_t sys_dup2;
    sys_dup_t sys_dup;
    sys_fcntl_t sys_fcntl;
};

struct fs_ops_ivk_list_t
{
    uint16_t fs_magic;
    struct fs_ops_t fs_ops;
};

extern struct fs_ops_ivk_list_t fs_ops_ivk_list[];

#endif //SYSDARFT_FS_H
