#ifndef SYSDARFT_SYS_UTSNAME_H_
#define SYSDARFT_SYS_UTSNAME_H_

#include <sys/types.h>

struct utsname {
    char sysname    [9];
    char nodename   [9];
    char release    [9];
    char version    [9];
    char machine    [9];
};

extern int uname(struct utsname *);

#endif // SYSDARFT_SYS_UTSNAME_H_
