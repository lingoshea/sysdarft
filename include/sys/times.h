#ifndef SYSDARFT_TIMES_H_
#define SYSDARFT_TIMES_H_

#include <sys/types.h>

struct tms {
    time_t tms_utime;
    time_t tms_stime;
    time_t tms_cutime;
    time_t tms_cstime;
};

struct utime_t
{
    unsigned long _sec;
    unsigned long _msec;
};

extern time_t times(struct tms *);

#endif // SYSDARFT_TIMES_H_
