#ifndef SYSDARFT_SYS_TYPES_H_
#define SYSDARFT_SYS_TYPES_H_

#ifndef _SIZE_T
# define _SIZE_T
typedef unsigned long int size_t;
#endif // _SIZE_T

#ifndef _TIME_T
# define _TIME_T
typedef long time_t;
#endif // _TIME_T

#ifndef _PTRDIFF_T
# define _PTRDIFF_T
typedef long ptrdiff_t;
#endif // _PTRDIFF_T

#ifndef NULL
# define NULL ((void *) 0)
#endif

typedef long int        pid_t;
typedef unsigned short  uid_t;
typedef unsigned char   gid_t;
typedef unsigned short  dev_t;
typedef unsigned short  block_t;
typedef unsigned short  ino_t;
typedef unsigned short  mode_t;
typedef unsigned short  umode_t;
typedef unsigned char   nlink_t;
typedef int             daddr_t;
typedef long            off_t;
typedef unsigned char           uint8_t;
typedef unsigned short          uint16_t;
typedef unsigned long int       uint32_t;
typedef unsigned long long int  uint64_t;
typedef signed char             int8_t;
typedef signed short            int16_t;
typedef signed long int         int32_t;
typedef signed long long int    int64_t;

typedef uint32_t    DWORD;
typedef uint16_t    WORD;
typedef uint8_t     BYTE;
typedef DWORD   dword;
typedef WORD    word;
typedef BYTE    byte;

typedef long long int ssize_t;
typedef int fd_t;

typedef struct { int quot, rem; }    div_t;
typedef struct { long quot,rem; }   ldiv_t;

struct ustat {
    daddr_t f_tfree;
    ino_t   f_tinode;
    char    f_fname[6];
    char    f_fpack[6];
};

#endif // SYSDARFT_SYS_TYPES_H_
