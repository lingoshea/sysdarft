#ifndef SYSDARFT_STDIO_H_
#define SYSDARFT_STDIO_H_

#include <stdarg.h>

/* simple implementation for functions used in standard C */

int __f_printf(char *, const char *, va_list);
int printf(const char *, ...);
int __f_vsprintf(char * buf, const char * fmt, ...);
int vsprintf(char *, const char *, va_list);

#endif // SYSDARFT_STDIO_H_
