#ifndef SYSDARFT_STDLIB_H_
#define SYSDARFT_STDLIB_H_

#include <unistd.h>

int isalpha(int);
int isspace(int);
int ispunct(int);
int tolower(int);
int isupper(int);
int isdigit(int);

long strtol(const char *, char **, register int);

#endif // SYSDARFT_STDLIB_H_
