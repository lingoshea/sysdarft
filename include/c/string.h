#ifndef SYSDARFT_STRING_H_
#define SYSDARFT_STRING_H_

#ifndef NULL
#define NULL ((void *) 0)
#endif

#ifndef _SIZE_T
#define _SIZE_T
typedef unsigned int size_t;
#endif

extern char * strerror(int errno);

/*
 * This string-include defines all string functions as inline
 * functions. Use gcc. It also assumes ds=es=data space, this should be
 * normal. Most of the string-functions are rather heavily hand-optimized,
 * see especially strtok,strstr,str[c]spn. They should work, but are not
 * very easy to understand. Everything is done entirely within the register
 * set, making the functions fast and clean. String instructions have been
 * used through-out, making for "slightly" unclear code :-)
 */
 
extern inline char * strcpy(char *, const char *);
extern inline char * strcat(char *, const char *);
extern inline int strcmp    (const char *, const char *);
extern inline int strspn    (const char *, const char *);
extern inline int strcspn   (const char *, const char *);
extern inline char * strpbrk(const char *, const char *);
extern inline char * strstr (const char *, const char *);
extern inline int strlen    (const char *);
extern inline char * strchr (const char *, char);
extern inline char * strncpy(char *dest, const char *src, unsigned int count);
extern char * ___strtok;

extern inline char * strtok (char *, const char *);

/*
 * Changes by falcon<zhangjinw@gmail.com>, the original return value is static
 * inline ... it can not be called by other functions in another files.
 */

extern inline void * memcpy  (void *dest, const void *src, unsigned int n);
extern inline void * memmove (void *dest, const void *src, unsigned int n);
extern inline void * memchr  (const void *cs, char c, unsigned int count);
static inline void * memset  (void *s, char c, unsigned int count);
static inline int    memcmp  (const void *cs, const void *ct, unsigned int count);
#endif // SYSDARFT_STRING_H_
