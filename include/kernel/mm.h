#ifndef SYSDARFT_MM_H_
#define SYSDARFT_MM_H_

#define PAGE_SIZE 4096

#include <sys/types.h>

extern unsigned long get_free_page (void);
extern unsigned long put_page (unsigned long, unsigned long);
extern void free_page (unsigned long);

void * malloc (unsigned int);
void   free_s (void *,  int);

#define free(x) free_s((x), 0)

void   copy_from_user  (void *, const void *, size_t);
void   copy_to_user    (void *, const void *, size_t);
void   verify_area     (void *, int);
void   cstr_copy_from_user (char *, const char *);

#endif // SYSDARFT_MM_H_
