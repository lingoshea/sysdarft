#ifndef SYSDARFT_KERNEL_H_
#define SYSDARFT_KERNEL_H_

/*
 * 'kernel.h' contains some often-used function prototypes etc
 */

_Noreturn
void  panic (const char *);
extern int panic_mode;

/*
 * This is defined as a macro, but at some point this might become a
 * real subroutine that sets a flag if it returns true (to do
 * BSD-style accounting where the process is flagged if it uses root
 * privs).  The implication of this is that you should do normal
 * permissions checks first, and check suser() last.
 */
#define suser() (current->euid == 0)

#define __kernel
#define __user
#define __unused

#endif // SYSDARFT_KERNEL_H_
