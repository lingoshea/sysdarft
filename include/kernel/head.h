#ifndef SYSDARFT_HEAD_H_
#define SYSDARFT_HEAD_H_

typedef struct desc_struct {
    unsigned long a, b;
} desc_table[256];

extern unsigned long _pg_dir[1024];
extern desc_table _idt, _gdt;

#define GDT_NUL     0
#define GDT_CODE    1
#define GDT_DATA    2
#define GDT_TMP     3

#define LDT_NUL     0
#define LDT_CODE    1
#define LDT_DATA    2

#endif // SYSDARFT_HEAD_H_
