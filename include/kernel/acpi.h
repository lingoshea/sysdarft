#ifndef SYSDARFT_ACPI_H_
#define SYSDARFT_ACPI_H_

#ifdef WITH_ACPI
int     init_acpi       (void);
void    acpi_power_off  (void);
int     acpi_enable     (void);

extern unsigned short s3_suspended;
#endif // WITH_ACPI

#endif // SYSDARFT_ACPI_H_
