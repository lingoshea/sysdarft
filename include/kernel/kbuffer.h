#ifndef SYSDARFT_KBUFFER_H_
#define SYSDARFT_KBUFFER_H_

#include <sys/types.h>
#include <kernel/kernel.h>

/*
 * kbuffer is a round buffer that overwrites old info when
 * buffer is full.
 * it's used in kmsg
 * */

typedef struct kbuffer_t
{
    char *   buffer;
    size_t   size;
    size_t   r_off;
    size_t   w_off;
    uint32_t flags;
    size_t   err_count;
} kbuffer_t;

#define KBUFFER_CLEAR    0b00000000000000000000000000000000 /* default state, buffer state clear */
#define KBUFFER_FULL     0b00000000000000000000000000000001 /* indicates buffer full when moving read offset */
#define KBUFFER_LOCK     0b00000000000000000000000000000010 /* disable writing in buffer */
#define KBUFFER_2USER    0b00000000000000000000000000000100 /* set kbuffer_read to userspace mode */
#define KBUFFER_KERNEL   0b00000000000000000000000000000000 /* set kbuffer_read to kernel space mode */

ssize_t kbuffer_read   (kbuffer_t *, char * __user, size_t);
ssize_t kbuffer_write  (kbuffer_t *, const char * __kernel, size_t);
void    kbuffer_init   (char *, size_t, kbuffer_t*);
size_t  kbuffer_leng   (kbuffer_t *);

#endif // SYSDARFT_KBUFFER_H_
