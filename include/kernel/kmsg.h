#ifndef SYSDARFT_KMSG_H_
#define SYSDARFT_KMSG_H_

#include <c/stdio.h>
#include <sys/types.h>
#include <kernel/kbuffer.h>

int  printk       (const char *, ...);
void kbuf_init    ();
int  __f_printk   (const char *, ...);
int c_write_tty   (const char *, int);
int str_write_tty (const char *);

const char * get_name_by_addr (uint32_t addr);
uint32_t cur_time_decimal(uint32_t);

extern kbuffer_t kmsg_buffer;

#define KMSG_NORMAL_COLOR   "\033[34;7;37m"
#define KMSG_WARNING_COLOR  "\033[31;7m"
#define K_WARNING KMSG_WARNING_COLOR
#define K_NORMAL KMSG_NORMAL_COLOR

#endif // SYSDARFT_KMSG_H_
