#ifndef SYSDARFT_KTRACE_H
#define SYSDARFT_KTRACE_H

#ifdef DEBUG

typedef void * fx_t;
void _ktrace_syscall_report();

#endif // DEBUG

#endif // SYSDARFT_KTRACE_H
