#ifndef SYSDARFT_ASM_IO_H_
#define SYSDARFT_ASM_IO_H_

/* Note that even though every single function here is marked
 * inline, GCC will simply ignore this and treat inline functions
 * like actual functions. define __IO_MARCO_INLINE__ to mark functions
 * here as actual inline function
 */

#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline unsigned char
inb (unsigned short int _port)
{
    unsigned char _v;
    __asm__ __volatile__ ("inb %w1, %0":"=a" (_v):"Nd" (_port));
    return _v;
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline unsigned char
inb_p (unsigned short int _port)
{
    unsigned char _v;
    __asm__ __volatile__ ("inb  %w1,    %0  \n"
                          "outb %%al,   $0x80"
                          :"=a" (_v):"Nd" (_port));
    return _v;
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline unsigned short int
inw (unsigned short int _port)
{
    unsigned short _v;
    __asm__ __volatile__ ("inw %w1, %0":"=a" (_v):"Nd" (_port));
    return _v;
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline unsigned short int
inw_p (unsigned short int _port)
{
    unsigned short int _v;
    __asm__ __volatile__ ("inw   %w1,   %0   \n"
                          "outb  %%al,  $0x80":
                          "=a" (_v):"Nd" (_port));
    return _v;
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline unsigned int
inl (unsigned short int _port)
{
    unsigned int _v;
    __asm__ __volatile__ ("inl %w1, %0":"=a" (_v):"Nd" (_port));
    return _v;
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline unsigned int
inl_p (unsigned short int _port)
{
    unsigned int _v;
    __asm__ __volatile__ ("inl  %w1,    %0  \n"
                          "outb %%al,   $0x80"
                          :"=a" (_v):"Nd" (_port));
    return _v;
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
outb (unsigned char _value, unsigned short int _port)
{
    __asm__ __volatile__ ("outb %b0, %w1": :"a" (_value), "Nd" (_port));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
outb_p (unsigned char _value, unsigned short int _port)
{
    __asm__ __volatile__ ("outb %b0,    %w1 \n"
                          "outb %%al,   $0x80"
                          : :"a" (_value),
                            "Nd" (_port));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
outw (unsigned short int _value, unsigned short int _port)
{
    __asm__ __volatile__ ("outw %w0,    %w1": :"a" (_value), "Nd" (_port));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
outw_p (unsigned short int _value, unsigned short int _port)
{
    __asm__ __volatile__ ("outw %w0,    %w1 \n"
                          "outb %%al,   $0x80"
                          : :"a" (_value),
                            "Nd" (_port));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
outl (unsigned int _value, unsigned short int _port)
{
    __asm__ __volatile__ ("outl %0, %w1": :"a" (_value), "Nd" (_port));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
outl_p (unsigned int _value, unsigned short int _port)
{
    __asm__ __volatile__ ("outl %0,     %w1 \n"
                          "outb %%al,   $0x80"
                          : :"a" (_value),
                            "Nd" (_port));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
insb (unsigned short int _port, void *_addr, unsigned long int _count)
{
    __asm__ __volatile__ ("cld ; rep ; insb"
                    :"=D" (_addr), "=c" (_count)
                    :"d" (_port), "0" (_addr), "1" (_count));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
insw (unsigned short int _port, void *_addr, unsigned long int _count)
{
    __asm__ __volatile__ ("cld ; rep ; insw"
                    :"=D" (_addr), "=c" (_count)
                    :"d" (_port), "0" (_addr), "1" (_count));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
insl (unsigned short int _port, void *_addr, unsigned long int _count)
{
    __asm__ __volatile__ ("cld ; rep ; insl"
                    :"=D" (_addr), "=c" (_count)
                    :"d" (_port), "0" (_addr), "1" (_count));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
outsb (unsigned short int _port, const void *_addr,
       unsigned long int _count)
{
    __asm__ __volatile__ ("cld ; rep ; outsb"
                    :"=S" (_addr), "=c" (_count)
                    :"d" (_port), "0" (_addr), "1" (_count));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
outsw (unsigned short int _port, const void *_addr,
       unsigned long int _count)
{
    __asm__ __volatile__ ("cld ; rep ; outsw"
                    :"=S" (_addr), "=c" (_count)
                    :"d" (_port), "0" (_addr), "1" (_count));
}


#ifdef __IO_MARCO_INLINE__
__attribute__((always_inline))
#endif // __IO_MARCO_INLINE__
static __inline void
outsl (unsigned short int _port, const void *_addr,
       unsigned long int _count)
{
    __asm__ __volatile__ ("cld ; rep ; outsl"
                    :"=S" (_addr), "=c" (_count)
                    :"d" (_port), "0" (_addr), "1" (_count));
}


#endif // SYSDARFT_ASM_IO_H_
