#ifndef SYSDARFT_UNISTD_H_
#define SYSDARFT_UNISTD_H_

#define _POSIX_VERSION 198808L

#define _POSIX_CHOWN_RESTRICTED /* only root can do a chown (I think..) */
#define _POSIX_NO_TRUNC         /* no pathname truncation (but see in kernel) */
#define _POSIX_VDISABLE '\0'    /* character to disable things like ^C */
/*#define _POSIX_SAVED_IDS */   /* we'll get to this yet */
/*#define _POSIX_JOB_CONTROL */ /* we aren't there quite yet. Soon hopefully */

#define STDIN_FILENO  0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2

#ifndef NULL
# define NULL ((void *)0)
#endif

/* access */
#define F_OK 0
#define X_OK 1
#define W_OK 2
#define R_OK 4

/* lseek */
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

/* reboot */
/* (In most modern UNIX systems halt is equal to shutdown) */
#define SYSRBT_SYSTEM_HALT     0 /* halt system */
#define SYSRBT_SYSTEM_SHUTDOWN 1 /* shutdown system */
#define SYSRBT_SYSTEM_REBOOT   2 /* reboot system */

/* _SC stands for System Configuration. We don't use them much */
#define _SC_ARG_MAX         1
#define _SC_CHILD_MAX       2
#define _SC_CLOCKS_PER_SEC  3
#define _SC_NGROUPS_MAX     4
#define _SC_OPEN_MAX        5
#define _SC_JOB_CONTROL     6
#define _SC_SAVED_IDS       7
#define _SC_VERSION         8

/* more (possibly) configurable things - now pathnames */
#define _PC_LINK_MAX        1
#define _PC_MAX_CANON       2
#define _PC_MAX_INPUT       3
#define _PC_NAME_MAX        4
#define _PC_PATH_MAX        5
#define _PC_PIPE_BUF        6
#define _PC_NO_TRUNC        7
#define _PC_VDISABLE        8
#define _PC_CHOWN_RESTRICTED 9

#include <sys/stat.h>
#include <sys/times.h>
#include <sys/utsname.h>
#include <utime.h>

#if (defined(__LIBRARY__) || defined(__KERNEL_SRC__))

# define __NR_setup      0 /* used only by init, to get system going */
# define __NR_exit       1
# define __NR_fork       2
# define __NR_read       3
# define __NR_write      4
# define __NR_open       5
# define __NR_close      6
# define __NR_waitpid    7
# define __NR_creat      8
# define __NR_link       9
# define __NR_unlink     10
# define __NR_execve     11
# define __NR_chdir      12
# define __NR_time       13
# define __NR_mknod      14
# define __NR_chmod      15
# define __NR_chown      16
# define __NR_break      17
# define __NR_stat       18
# define __NR_lseek      19
# define __NR_getpid     20
# define __NR_mount      21
# define __NR_umount     22
# define __NR_setuid     23
# define __NR_getuid     24
# define __NR_stime      25
# define __NR_ptrace     26
# define __NR_alarm      27
# define __NR_fstat      28
# define __NR_pause      29
# define __NR_utime      30
# define __NR_stty       31
# define __NR_gtty       32
# define __NR_access     33
# define __NR_nice       34
# define __NR_ftime      35
# define __NR_sync       36
# define __NR_kill       37
# define __NR_rename     38
# define __NR_mkdir      39
# define __NR_rmdir      40
# define __NR_dup        41
# define __NR_pipe       42
# define __NR_times      43
# define __NR_prof       44
# define __NR_brk        45
# define __NR_setgid     46
# define __NR_getgid     47
# define __NR_signal     48
# define __NR_geteuid    49
# define __NR_getegid    50
# define __NR_acct       51
# define __NR_phys       52
# define __NR_lock       53
# define __NR_ioctl      54
# define __NR_fcntl      55
# define __NR_mpx        56
# define __NR_setpgid    57
# define __NR_ulimit     58
# define __NR_uname      59
# define __NR_umask      60
# define __NR_chroot     61
# define __NR_ustat      62
# define __NR_dup2       63
# define __NR_getppid    64
# define __NR_getpgrp    65
# define __NR_setsid     66
# define __NR_sigaction  67
# define __NR_sgetmask   68
# define __NR_ssetmask   69
# define __NR_setreuid   70
# define __NR_setregid   71
# define __NR_reboot     72
# define __NR_query      73
#ifdef DEBUG
# define __NR_ktrace     74
# define __NR_ldmap      75
#endif // DEBUG

# define _syscall0(type, name)                                                 \
  type name(void) {                                                            \
    long __res;                                                                \
    __asm__ volatile("int $0x80" : "=a"(__res) : "0"(__NR_##name));            \
    if (__res >= 0)                                                            \
      return (type)__res;                                                      \
    errno = -__res;                                                            \
    return -1;                                                                 \
  }

# define _syscall1(type, name, atype, a)                                       \
  type name(atype a) {                                                         \
    long __res;                                                                \
    __asm__ volatile("int $0x80"                                               \
                     : "=a"(__res)                                             \
                     : "0"(__NR_##name), "b"((long)(a)));                      \
    if (__res >= 0)                                                            \
      return (type)__res;                                                      \
    errno = -__res;                                                            \
    return -1;                                                                 \
  }

# define _syscall2(type, name, atype, a, btype, b)                             \
  type name(atype a, btype b) {                                                \
    long __res;                                                                \
    __asm__ volatile("int $0x80"                                               \
                     : "=a"(__res)                                             \
                     : "0"(__NR_##name), "b"((long)(a)), "c"((long)(b)));      \
    if (__res >= 0)                                                            \
      return (type)__res;                                                      \
    errno = -__res;                                                            \
    return -1;                                                                 \
  }

# define _syscall3(type, name, atype, a, btype, b, ctype, c)                   \
  type name(atype a, btype b, ctype c) {                                       \
    long __res;                                                                \
    __asm__ volatile("int $0x80"                                               \
                     : "=a"(__res)                                             \
                     : "0"(__NR_##name), "b"((long)(a)), "c"((long)(b)),       \
                       "d"((long)(c)));                                        \
    if (__res >= 0)                                                            \
      return (type)__res;                                                      \
    errno = -__res;                                                            \
    return -1;                                                                 \
  }

#endif /* __LIBRARY__ || __KERNEL__SRC__ */

extern int errno;

int     access  (const char *filename, mode_t mode);
int     acct    (const char *filename);
int     alarm   (int sec);
int     brk     (void *end_data_segment);
void *  sbrk    (ptrdiff_t);
int     chdir   (const char *);
int     chown   (const char *, uid_t, gid_t);
int     chroot  (const char *);
int     close   (int);
int     creat   (const char *, mode_t);
int     dup     (int);
int     execve  (const char *, char **, char **);
int     execv   (const char *, char **);
int     execvp  (const char *, char **);
int     execl   (const char *, char *, ...);
int     execlp  (const char *, char *, ...);
int     execle  (const char *, char *, ...);
// volatile void exit(int status);
void    _exit   (int);
// volatile void _exit(int status);
int     fcntl   (int, int, ...);
static
int     fork    (void);
int     getpid  (void);
int     getuid  (void);
int     geteuid (void);
int     getgid  (void);
int     getegid (void);
int     ioctl   (int, int, ...);
int     kill    (pid_t, int);
int     link    (const char *, const char *);
int     lseek   (int, off_t, int);
int     mknod   (const char *, mode_t, dev_t );
int     mount   (const char *, const char *, int);
int     nice    (int);
int     open    (const char *, int, ...);
static
int     pause   (void);
int     pipe    (int *);
int     read    (int, char *, off_t);
int     setpgrp (void);
int     setpgid (pid_t, pid_t);
int     setuid  (uid_t);
int     setgid  (gid_t);
void (*signal   (int, void (*)(int)))   (int);
int     stime   (time_t *tptr);
static
int     sync    (void);
time_t  time    (time_t *);
int     ulimit  (int, long);
int     umount  (const char *);
int     unlink  (const char *);
int     ustat   (dev_t, struct ustat *);
pid_t   waitpid (pid_t, int *, int);
pid_t   wait    (int *);
int     write   (int, const char *, off_t);
int     dup2    (int, int);
int     getppid (void);
pid_t   getpgrp (void);
pid_t   setsid  (void);

#endif // SYSDARFT_UNISTD_H_
