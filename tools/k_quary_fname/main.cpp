#include <map>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <cstring>

/* This program is used to find specific symbol by given address
 * a System.map is needed for operation, which should be generated automatically
 * along with kernel itself.
 * */

struct symbol_t
{
    char        type;
    std::string name;
};

typedef std::vector < symbol_t > symbol_tree_branch_t;
typedef std::map < uint64_t, symbol_tree_branch_t > symbol_tree_t;

#define HELP        " [SYSTEM_MAP]"

#define BLACK       "\033[30m"
#define RED         "\033[31m"
#define GREEN       "\033[32m"
#define YELLOW      "\033[33m"
#define BLUE        "\033[34m"
#define PURPLE      "\033[35m"
#define LIGHT_BLUE  "\033[36m"
#define RESET       "\033[0m"

void help(char * argv0)
{
    std::cout << GREEN << argv0 << BLUE HELP RESET << std::endl;
    exit(EXIT_SUCCESS);
}

int main(int argc, char ** argv)
{
    if (argc < 2)
    {
        help(*argv);
    }

    std::ifstream System_map(argv[1]);
    std::string addr, name;
    char type;
    uint64_t b_addr;
    symbol_tree_t sysmap_tree;

    if (!System_map)
    {
        std::cerr << "Cannot open file `" << argv[1] << "`, " << strerror(errno) << " (errno flag)" << std::endl;
        help(*argv);
    }

    while (System_map >> addr >> type >> name)
    {
        b_addr = strtoull(addr.c_str(), nullptr, 16);
        sysmap_tree[b_addr].emplace_back((symbol_t){ .type = type, .name = name});
    }

    int addr_count = argc - 2;
    int it = 2;
    bool tag_outta_range;
    bool started_from_console = addr_count > 0;

    while ( addr_count > 0 || (std::cout << "?> " << std::flush && std::getline(std::cin, addr)) )
    {
        tag_outta_range = false;

        if (addr_count > 0)
        {
            addr = argv[it++];
            addr_count--;
        }

        if (addr.empty()) {
            continue;
        }

        b_addr = strtoll(addr.c_str(), nullptr, 16);
        for (auto iterator = sysmap_tree.begin(); iterator != sysmap_tree.end();)
        {
            auto located_addr = iterator->first;
            auto located_map  = iterator->second;
            iterator++;
            if (located_addr <= b_addr && iterator->first > b_addr)
            {
                printf(GREEN "(+)  " YELLOW "Target(s) found in memory region\n"
                       "       [" PURPLE "0x%08lX" YELLOW " - " PURPLE "0x%08lX" YELLOW "]\n",
                       located_addr, iterator->first - 1);
                fflush(stdout);
                for (auto &j : located_map)
                {
                    if (j.type != 'T' && j.type != 't')
                    {
                        std::cout << "    " << BLACK "(" << j.type << ")\t" << j.name << "\t(not in.text)\n" << RESET;
                        continue;
                    }

                    std::cout << "    " << YELLOW "(" BLUE << j.type << YELLOW ")\t" LIGHT_BLUE << j.name << RESET "\n";
                }

                std::cout << std::endl;

                tag_outta_range = true;
                break;
            }
        }

        if (!tag_outta_range)
        {
            printf(RED "(-)  " YELLOW "Target (" RED "0x%08lX" YELLOW ") out of range\n" RESET "\n", b_addr);
            fflush(stdout);
        }

        if (addr_count == 0 && started_from_console)
        {
            break;
        }
    }

    return 0;
}
