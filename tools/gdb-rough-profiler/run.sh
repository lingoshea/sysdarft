#!/usr/bin/env bash
# argv: "Build Directory" "Kernel Symbol File" "System.map" "Source Directory" "Port"

MYDIR="$(dirname "$(realpath "$0")")"
RAW="$(grep -E --include="*.s" --include="*.S" "*\:" -R "$4")"
_BLACKLIST="$(echo -e "main\n${RAW//:/ }" | awk ' { print $2 } ' | sort | uniq | grep -v '\.\|\*\|#' | grep -vx "[0-9]")"

# env vars are not necessarily empty. You can pre-set these
# just leave \| at the end of BLACKLIST
export BLACKLIST
export WHITELIST
export NAMEFILTER
export FILTERMODE
export GDBPORT

GDBPORT="$5"
BLACKLIST="$BLACKLIST"
WHITELIST="$WHITELIST"
NAMEFILTER="$NAMEFILTER"

if [ -z "$FILTERMODE" ]; then
    FILTERMODE="include"
fi

if [ -z "$INPUTMODE" ]; then
    FILTERMODE="exclude"
fi

for NAME in $_BLACKLIST
do
    BLACKLIST="$BLACKLIST""$NAME\|"
done

for NAME in $WHITELIST
do
    BLACKLIST=${BLACKLIST/"$NAME\|"/}
done

BLACKLIST=${BLACKLIST:0:${#BLACKLIST}-2}
LIST=$(awk "{ if (\$2 == \"t\" || \$2 == \"T\") { print \$3; } }" < "$1/$3" | sort | uniq | grep -xv "$BLACKLIST")
# make sure that CMD is not empty
CMD=$(echo -ne "set output-radix 16\n")

add_cmd()
{
    NAME="$1"
    CMD=$(echo -ne "$CMD\n""break $NAME\n")
}

if [ "$INPUTMODE" = "include" ]; then
    LIST="$WHITELIST"
fi

for NAME in $LIST;
do
    if [ "$FILTERMODE" == "include" ]; then
        if echo "$NAME" | grep -E "$NAMEFILTER" > /dev/null 2> /dev/null; then
            add_cmd "$NAME"
        fi
    elif [ "$FILTERMODE" == "exclude" ]; then
        if ! echo "$NAME" | grep -E "$NAMEFILTER" > /dev/null 2> /dev/null; then
            add_cmd "$NAME"
        fi
    fi
done

export CMD
export LIST

env DIR="$MYDIR" RUN="$1" KNAME="$2" gdb -x "$MYDIR"/gdb-step-run.gdb -ex="quit"
