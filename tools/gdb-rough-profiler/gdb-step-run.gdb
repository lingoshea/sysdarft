python
import os
gdb.execute("source " + os.environ["DIR"] + "/gef/gef.py")

blacklist = os.environ["BLACKLIST"]
blacklist = blacklist.replace("\\|", "\n")
blacklist = "Blacklisted the following symbol(s):\n" + blacklist + "\n"

whitelist = os.environ["WHITELIST"]
whitelist = whitelist.replace("\\|", "\n")
whitelist = "Whitelisted the following symbol(s):\n" + whitelist + "\n"

filter = "Filter: " + os.environ["NAMEFILTER"]
filter_mode = "Filter mode: " + os.environ["FILTERMODE"]

brk_points = "\nBreakpoints: \n" + os.environ["LIST"]

fd = open(os.environ["RUN"] + "/context.txt", "w")
fd.write(blacklist)
fd.write(whitelist)
fd.write(filter_mode)
fd.write(filter)
fd.write(brk_points)
fd.close()

print(blacklist)
print(whitelist)
print(filter_mode)
print(filter)
print(brk_points)

gdb.execute("set logging file " + os.environ["RUN"] + "/context.txt")
gdb.execute("file " + os.environ["RUN"] + "/" + os.environ["KNAME"])
gdb.execute(os.environ["CMD"])
end

set step-mode on
set logging on
set verbose on
set confirm on
break main

python
import os
gdb.execute("target remote :" + os.environ["GDBPORT"])
end

continue

while (1)
    continue
end
