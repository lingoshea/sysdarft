#!/usr/bin/env bash

FDB_IMG="$1"

if [ -e "${FDB_IMG}" ]; then
    BYTES=$(du --bytes "${FDB_IMG}" | awk ' { print $1 } ')
    if [ "$BYTES" -eq 1474560 ]; then
        echo "File clean. Ignored..."
        exit 0;
    fi
fi

dd if=/dev/zero of="${FDB_IMG}" bs=1024 count=1440
