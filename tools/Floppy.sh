#!/usr/bin/env bash

[ -z "$1" ] && exit 1

FILE_NAME="$1"
FLOPPY_SIZE=1474560
FILE_SIZE=$(du --bytes "$FILE_NAME" | awk ' { print $1 } ')
APPEND_SIZE=$(echo "$FLOPPY_SIZE - $FILE_SIZE" | bc)

cp "$FILE_NAME" "$FILE_NAME.img"
dd if=/dev/zero of="$FILE_NAME.img" bs=1 skip="$FILE_SIZE" count="$APPEND_SIZE" conv=notrunc oflag=append
