#!/usr/bin/env python3

import sys
import PyQt5.QtWidgets
import PyQt5.QtCore
import PyQt5.QtGui
import os

fd = open(sys.argv[1])

var_list = {}

marco = ""
var = ""
current_state = "source"
cmake_source_dir = ""
cmake_binary_dir = ""

for chunk in iter(lambda: fd.read(1), ''):
    # switch state
    if chunk == ':':
        if current_state == "marco":
            current_state = "variable"
        else:
            current_state = "marco"

        continue

    if chunk == '\n':
        # switch state
        if current_state == "source":
            current_state = "binary"
            continue

        current_state = "marco"

        if len(marco) != 0:
            # record
            var_list[marco] = var

        # clear buffer
        marco = ""
        var = ""
        continue

    if current_state == "marco":
        marco += str(chunk)

    if current_state == "variable":
        var += str(chunk)

    if current_state == "source":
        cmake_source_dir += str(chunk)

    if current_state == "binary":
        cmake_binary_dir += str(chunk)

fd.close()


def console():
    skip_exec_def_v = input("Skip execution definition marco?")
    skip_exec_def = skip_exec_def_v.lower() == "y" or skip_exec_def_v.lower() == "yes" \
                    or skip_exec_def_v.lower() == "yep" or len(skip_exec_def_v) == 0

    for vmarco in var_list:
        if vmarco[-5:] == "_EXEC" and skip_exec_def:
            continue

        vvar = input(vmarco + "=" + var_list[vmarco] + " (Changing?)")
        if len(vvar) == 0 or vvar.lower() == "n" or vvar.lower() == "no" or vvar.lower() == "nope":
            continue

        if vvar.lower() == "y" or vvar.lower() == "yes" or vvar.lower() == "yep":
            vvar = input("New marco: ")

        var_list[vmarco] = vvar

    return 0


def mkconfig(_var_list):
    cmake_param = ""
    for vmarco in _var_list:
        cmake_param += " -D " + vmarco + "=\"" + _var_list[vmarco] + "\""

    return cmake_param


if console() != 0:
    sys.exit(1)


os.system("cmake " + cmake_source_dir + mkconfig(var_list))
