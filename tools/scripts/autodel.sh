#!/usr/bin/env bash
# argv: "Token" "Limit"

MYDIR="$(dirname "$(realpath "$0")")"
PROJECT_ID="$(curl --header "Private-Token: $1" -X GET "https://gitlab.com/api/v4/projects?search=sysdarft" 2> /dev/null | python3 -c 'import sys, json; print(json.load(sys.stdin)[0]["id"])')"

"$MYDIR/autodel-pipeline.py" "$PROJECT_ID" "$1" "$2"
