#!/usr/bin/env python3
import sys
import requests


def info(fmt):
    print("\033[32;1mINFO:    " + "\033[0m" + str(fmt))


def warn(fmt):
    print("\033[33;1mWARNING: " + "\033[0m" + str(fmt))


def error(fmt):
    print("\033[31;1mERROR:   " + "\033[0m" + str(fmt))
    exit(1)


if len(sys.argv) != 4:
    error("Invalid arguments")

prj_id = str(sys.argv[1])       # 25305999
prv_token = str(sys.argv[2])    # token
limit = int(sys.argv[3])        # job limit

headers = {
    "PRIVATE-TOKEN": prv_token,
}

url = "https://gitlab.com/api/v4/projects/" + prj_id + "/pipelines"

response = requests.get(url, headers).json()
pipeline_list = []

if len(response) == 0:
    error("Empty response. No pipeline job.")

if "message" in response:
    error(response["message"])

for pipeline in response:
    pipeline_list.append(pipeline["id"])

pipeline_list.sort(reverse=True)

if len(pipeline_list) > limit:
    pipeline_list = pipeline_list[limit:]
else:
    info("Maximum job limit(" + str(limit) + ") not reached.")
    info("Exiting ...")
    exit()

for job in pipeline_list:
    info(requests.delete(url + "/" + str(job), headers=headers))
