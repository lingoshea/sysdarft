#ifdef DEBUG

#include <kernel/ktrace.h>
#include <kernel/sys.h>

int sys_ktrace(void)
{
    _ktrace_syscall_report();
    return 0;
}

#endif // DEBUG
