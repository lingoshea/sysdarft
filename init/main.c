/*
 *  init/main.c
 */

#define __LIBRARY__
#include <time.h>
#include <unistd.h>

/*
 * we need this inline - forking from kernel space will result
 * in NO COPY ON WRITE (!!!), until an execve is executed. This
 * is no problem, but for the stack. This is handled by not letting
 * main() use the stack at all after fork(). Thus, no function
 * calls - which means inline code for fork too, as otherwise we
 * would use the stack upon exit from 'fork()'.
 *
 * Actually only pause and fork are needed inline, so that there
 * won't be any messing with the stack from main(), but we define
 * some others too.
 */

// these 2 statement is required:
static inline fork  (void) __attribute__((always_inline));
static inline pause (void) __attribute__((always_inline));

static inline _syscall0(int, fork)
static inline _syscall0(int, pause)
static inline _syscall1(int, setup, void *, BIOS)
static inline _syscall0(int, sync)
static inline _syscall1(int, reboot, int, command)
static inline _syscall3(int, query, char *, buf, size_t, len, int, cmd)
inline _syscall2(int, mkdir, const char *, pname, mode_t, mode)
inline _syscall1(int, uname, struct utsname *, command)


#include <c/stdio.h>
#include <asm/io.h>
#include <asm/system.h>
#include <kernel/sched.h>
#include <kernel/tty.h>
#include <fs/fcntl.h>
#include <c/stddef.h>
#include <kernel/fs.h>
#include <kernel/acpi.h>
#include <kernel/kmsg.h>

extern void init        (void);
extern void blk_dev_init(void);
extern void chr_dev_init(void);
extern void hd_init     (void);
extern void floppy_init (void);
extern void mem_init    (unsigned long start_mem, unsigned long end_mem);
extern void zn_dev_init ();
extern long kernel_mktime(struct tm *tm);

/*
 * This is set up by the setup-routine at boot-time
 */
#define EXT_MEM_K       (*(unsigned short *)    0x90002)
#define DRIVE_INFO      (*(struct drive_info *) 0x90080)
#define ORIG_ROOT_DEV   (*(unsigned short *)    0x901FC)

/*
 * Yeah, yeah, it's ugly, but I cannot find how to do this correctly
 * and this seems to work. I anybody has more info on the real-time
 * clock I'd be interested. Most of this was trial and error, and some
 * bios-listing reading. Urghh.
 */

#define CMOS_READ(addr)                 \
    ({                                  \
        outb_p(0x80 | (addr), 0x70);    \
        inb_p(0x71);                    \
    })

#define BCD_TO_BIN(val) ((val) = ((val)&15) + ((val) >> 4) * 10)

static void time_init(void)
{
    struct tm time;

    do
    {
        time.tm_sec  = CMOS_READ(0);
        time.tm_min  = CMOS_READ(2);
        time.tm_hour = CMOS_READ(4);
        time.tm_mday = CMOS_READ(7);
        time.tm_mon  = CMOS_READ(8);
        time.tm_year = CMOS_READ(9);
    } while (time.tm_sec != CMOS_READ(0));
    BCD_TO_BIN(time.tm_sec);
    BCD_TO_BIN(time.tm_min);
    BCD_TO_BIN(time.tm_hour);
    BCD_TO_BIN(time.tm_mday);
    BCD_TO_BIN(time.tm_mon);
    BCD_TO_BIN(time.tm_year);
    time.tm_mon--;
    startup_time = kernel_mktime(&time);
}

static unsigned long memory_end = 0;
static unsigned long buffer_memory_end = 0;
static unsigned long main_memory_start = 0;

struct drive_info
{
    char dummy[32];
} drive_info;

extern void pipeline_start(void);

_Noreturn
void main(void) /* This really IS void, no error here. */
{               /* The startup routine assumes (well, ...) this */
    /*
     * Interrupts are still disabled. Do necessary setups, then
     * enable them
     */

    ROOT_DEV   = ORIG_ROOT_DEV;
    drive_info = DRIVE_INFO;
    memory_end = (1 << 20) + (EXT_MEM_K << 10);
    memory_end &= 0xfffff000;

    //__asm__("jmp .");

    if (memory_end > 16 * 1024 * 1024) {
        memory_end = 16 * 1024 * 1024;
    }

    if (memory_end > 12 * 1024 * 1024) {
        buffer_memory_end = 4 * 1024 * 1024;
    }
    else if (memory_end > 8 * 1024 * 1024) {
        buffer_memory_end = 2 * 1024 * 1024;
    } else {
        buffer_memory_end = 1 * 1024 * 1024;
    }

    main_memory_start = buffer_memory_end;

    mem_init(main_memory_start, memory_end);
    kbuf_init();
    trap_init();
    blk_dev_init();
    chr_dev_init();
    tty_init();

    __f_printk("tty is now initialized in protected mode\n");

    __f_printk("Initializing system clock ... ");
    time_init();
    __f_printk("done.\n");

    printk("Initializing sched ... ");
    sched_init();
    __f_printk("done.\n");

    printk("Initializing buffer space ... ");
    buffer_init(buffer_memory_end);
    __f_printk("done.\n");

    printk("Initializing HDD ... ");
    hd_init();
    __f_printk("done.\n");

    printk("Initializing floppy driver ... ");
    floppy_init();
    __f_printk("done.\n");

    printk("Initializing virtual block device ... ");
    zn_dev_init(); // dev/null and /dev/zero
    __f_printk("done.\n");

#ifdef WITH_ACPI
    printk("Attempting to enable ACPI\n");
    init_acpi();
    acpi_enable();
#endif // WITH_ACPI

    printk("System init sequence finished\n");
    printk("init is now entering usermode\n");

    // Note that debug is not possible
    // for code inside userspace
    sti();
    move_to_user_mode();

    if (!fork())
    { /* we count on this going ok */
        init();
    }
    /*
   *   NOTE!!   For any other task 'pause()' would mean we have to get a
   * signal to awaken, but task0 is the sole exception (see 'schedule()')
   * as task 0 gets activated at every idle moment (when no other tasks
   * can run). For task0 'pause()' just means we go check if some other
   * task can run, and if not we return here.
   */
    for (;;) {
        pause();
    }
}

static char *argv_rc[] = {"/bin/sh", NULL};
static char *envp_rc[] = {"HOME=/", NULL};

static char *argv[] = {"-/bin/sh", NULL};
static char *envp[] = {"HOME=/usr/root", NULL};

static char kmsg[4096];
void save_kmsg();

void init(void)
{
    int pid, i;
    struct utsname sys_uname;

    setup((void *)&drive_info);
    (void)open("/dev/tty0", O_RDWR, 0);
    (void)dup(0);
    (void)dup(0);

    uname(&sys_uname);
    // printf("\x1b\x5b\x48\x1b\x5b\x32\x4a\x1b\x5b\x33\x4a");
    printf("\033[32;1m"
           "Welcome to \033[33;1m%s\033[32;1m "
           "(version \033[33;1m%s\033[32;1m) on "
           "\033[33;1m%s\033[32;1m\n\r",
           sys_uname.sysname,
           sys_uname.version,
           sys_uname.machine);

    printf("Currently we have \033[33;1m%d\033[32;1m\tbytes buffer space (\033[33;1m%d\033[32;1m buffers)\n"
           "                  \033[33;1m%d\033[32;1m\tbytes memory space\n\r"
           "\033[0m",
           NR_BUFFERS * BLOCK_SIZE,
           NR_BUFFERS,
           memory_end - main_memory_start);

    if ( !(pid = fork()) )
    {
        close(0);
        if (open("/etc/rc", O_RDONLY, 0))
        {
            _exit(1);
        }
        execve("/bin/sh", argv_rc, envp_rc);
        _exit(2);
    }

    if (pid > 0) {
        while (pid != wait(&i));
    }

    if ((pid = fork()) < 0)
    {
        printf("Fork failed in init!\r\n");
        _exit(1);
    }

    if (!pid)
    {
        close(0);
        close(1);
        close(2);
        setsid();
        (void)open("/dev/tty0", O_RDWR, 0);
        (void)dup(0);
        (void)dup(0);
#ifdef __PIPELINE__
        pipeline_start();
        _exit(0);
#else // __PIPELINE__
        _exit(execve("/bin/sh", argv, envp));
#endif // __PIPELINE__
    }

    while (pid != wait(&i));

    // reaching this code means user logged out

#ifdef SAVE_KMSG
    // try to save system log
    save_kmsg();

    // fucking 7 syncs in a row, filesystem should be synced by now
    sync(); sync(); sync(); sync(); sync(); sync(); sync();
#endif // SAVE_KMSG

#ifndef __PIPELINE__
    char buff;
    printf("\033[1mPress Enter to shutdown the system ...\n\033[0m");
    read(0, &buff, 1);
#endif // __PIPELINE__

    // system shutdown
    reboot(SYSRBT_SYSTEM_SHUTDOWN);

    // unreachable code if reboot worked
    printf("Reboot request failed! Halting system ...\n");
    // halt system if reboot failed
    __asm__("jmp .");
}

void save_kmsg()
{
    // mkdir query, ignore return code
    mkdir("/var", 0700);
    mkdir("/var/log", 0700);
    mkdir("/var/log/kmsg", 0700);

    __f_vsprintf(kmsg, "/var/log/kmsg/%d", CURRENT_TIME);
    fd_t fd = open(kmsg, O_WRONLY | O_CREAT);
    if (fd < 0)
    {
        printf("WARNING: Kmsg not saved!\n");
    }
    ssize_t len = query(kmsg, sizeof (kmsg), 0);
    write(fd, kmsg, len);
    close(fd);
}
