/*
 * init/lact.c
 *
 * this file provides linear activation in booting
 * after system is loaded, the system will activate according to
 * the system activation chart provided here
 */

typedef int (*sys_act_t) (void*);

sys_act_t activation_chart[] = {};
