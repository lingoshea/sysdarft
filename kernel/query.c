#include <kernel/query.h>
#include <kernel/kmsg.h>
#include <kernel/mm.h>

int sys_query(char * buf, size_t len, int cmd)
{
    int ret;

    verify_area(buf, len);

    switch (cmd)
    {
        case SYSQRY_ASK_KMSG:
            ret = kbuffer_read(&kmsg_buffer, buf, len);
            break;

        default:
            ret = -1;
    }

    return ret;
}
