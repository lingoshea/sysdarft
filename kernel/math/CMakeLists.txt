cmake_minimum_required(VERSION ${__MANUAL_SET_CMAKE_MINIMUM_REQUIRED_VERSION__})

set(LIB_NAME math)

project(${LIB_NAME} C)

set(INCLUDE_DIR "${SOURCE_ROOT_DIR}/include")

add_library(${LIB_NAME} STATIC math_emulate.c)
target_compile_options(${LIB_NAME} PUBLIC ${GCCFLAGS})
target_include_directories(${LIB_NAME} PUBLIC "${INCLUDE_DIR}")
set(KERNEL_CODE_DEP ${KERNEL_CODE_DEP} ${LIB_NAME} PARENT_SCOPE)
