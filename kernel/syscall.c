#include <kernel/sys.h>
#include <kernel/kmsg.h>
#include <kernel/ktrace.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

fn_ptr sys_call_table[] = { sys_setup, sys_exit, sys_fork, sys_read,
                            sys_write, sys_open, sys_close, sys_waitpid, sys_creat, sys_link,
                            sys_unlink, sys_execve, sys_chdir, sys_time, sys_mknod, sys_chmod,
                            sys_chown, sys_break, sys_stat, sys_lseek, sys_getpid, sys_mount,
                            sys_umount, sys_setuid, sys_getuid, sys_stime, sys_ptrace, sys_alarm,
                            sys_fstat, sys_pause, sys_utime, sys_stty, sys_gtty, sys_access,
                            sys_nice, sys_ftime, sys_sync, sys_kill, sys_rename, sys_mkdir,
                            sys_rmdir, sys_dup, sys_pipe, sys_times, sys_prof, sys_brk, sys_setgid,
                            sys_getgid, sys_signal, sys_geteuid, sys_getegid, sys_acct, sys_phys,
                            sys_lock, sys_ioctl, sys_fcntl, sys_mpx, sys_setpgid, sys_ulimit,
                            sys_uname, sys_umask, sys_chroot, sys_ustat, sys_dup2, sys_getppid,
                            sys_getpgrp, sys_setsid, sys_sigaction, sys_sgetmask, sys_ssetmask,
                            sys_setreuid,sys_setregid, sys_reboot, sys_query,
#ifdef DEBUG
                            sys_ktrace, sys_ldmap,
#endif // DEBUG

};

__asm__(".global nr_system_calls, big_kernel_lock;"
        // son of the fucking bitch, that auto size thing doesn't work
        // tried to adjust it, but these mem addr seems not quite accessible for syscall routines
        // i'll try other route later cuz this thing is too ugly
#ifdef DEBUG
        ".equ nr_system_calls, 75;"
#else // DEBUG
        ".equ nr_system_calls, 73;"
#endif // DEBUG
        );

// prevent parallel syscall
/* actually parallel won't happen because
 * this kernel only supports one core
 * however this will be added sooner or later
 * until the parallel syscall routine is implemented
 */
uint32_t big_kernel_lock = 0;

#ifdef DEBUG

void _ktrace_syscall_report()
{
}

#endif // DEBUG
