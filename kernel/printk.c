/*
 *  kernel/printk.c
 */

/*
 * When in kernel-mode, we cannot use printf, as fs is liable to
 * point to 'interesting' things. Make a printf with fs-saving, and
 * all is well.
 */

#include <stdarg.h>
#include <c/stdio.h>
#include <unistd.h>
#include <kernel/kbuffer.h>
#include <kernel/sched.h>
#include <kernel/kmsg.h>

kbuffer_t kmsg_buffer;

static char buf [256];
static char buf_procd [256];
static char * kmsg_raw_buff = NULL;

#define KMSG_BUFF_SIZE (1024 * 4)

int __f_vsprintf(char * buf, const char * fmt, ...)
{
    va_list args;
    int len;

    va_start(args, fmt);
    len = vsprintf(buf, fmt, args);
    va_end(args);

    return len;
}

int c_write_tty(const char * buff, int len)
{
    __asm__("push     %%fs          \n\t"
            "push     %%ds          \n\t"
            "pop      %%fs          \n\t"
            "pushl    %0            \n\t"
            "pushl    %1            \n\t"
            "pushl    $0            \n\t"
            "call     tty_write     \n\t"
            "addl     $8,   %%esp   \n\t"
            "popl     %0            \n\t"
            "pop      %%fs          \n\t"
    ::"r"(len), "r"(buff)
    : "ax", "cx", "dx");

    return len;
}

int str_write_tty(const char * buff)
{
    register int len = 0;
    while (buff[len])
    {
        len++;
    }

    __asm__("push     %%fs          \n\t"
            "push     %%ds          \n\t"
            "pop      %%fs          \n\t"
            "pushl    %0            \n\t"
            "pushl    %1            \n\t"
            "pushl    $0            \n\t"
            "call     tty_write     \n\t"
            "addl     $8,   %%esp   \n\t"
            "popl     %0            \n\t"
            "pop      %%fs          \n\t"
    ::"r"(len), "r"(buff)
    : "ax", "cx", "dx");

    return len;
}

void kbuf_init()
{
    kmsg_raw_buff = malloc(KMSG_BUFF_SIZE);

    if (kmsg_raw_buff == 0)
    {
        panic("!! WARNING !! Kernel message buffer not allocated");
    }

    kbuffer_init(kmsg_raw_buff, KMSG_BUFF_SIZE, &kmsg_buffer);
    kmsg_buffer.flags |= KBUFFER_2USER;
}

/* similar to printk, but without adding utime and caller.
 */
int __f_printk(const char *fmt, ...)
{
    va_list args;
    int len;

    va_start(args, fmt);
    vsprintf(buf, fmt, args);
    va_end(args);

    if (panic_mode)
    {
        len = __f_vsprintf(buf_procd, KMSG_WARNING_COLOR
                                      "%s\033[0m", buf);
    }
    else
    {
        len = __f_vsprintf(buf_procd, KMSG_NORMAL_COLOR
                                      "%s\033[0m", buf);
    }


    if (kmsg_raw_buff)
    {
        kbuffer_write(&kmsg_buffer, buf_procd, len);
    }

#ifndef KMSG_QUIET
    c_write_tty(buf_procd, len);
#endif // KMSG_QUIET

    return len;
}

uint32_t cur_time_decimal(uint32_t _jiffies)
{
    if (_jiffies > 1000)
    {
        _jiffies %= 1000;
    }

    return _jiffies;
}

/* Print message from kernel. Message will be recorded inside kernel
 * along with utime and caller. msg will also be printed onto the screen.
 * use marco KMSG_QUIET to suppress on screen kmsg
 */
int printk(const char * fmt, ...)
{
    va_list args;
    int len;

    va_start(args, fmt);
    vsprintf(buf,  fmt, args);
    va_end(args);

    // split color code seems to be causing bugs
    if (panic_mode)
    {
        len = __f_vsprintf(buf_procd,
                           KMSG_WARNING_COLOR
                           "[%d.%03d] <PANIC>: %s\033[0m",
                           CURRENT_TIME, cur_time_decimal(jiffies),
                           buf);

    }
    else
    {
        len = __f_vsprintf(buf_procd,
                           KMSG_NORMAL_COLOR
                           "[%d.%03d] [%s]: %s\033[0m",
                           CURRENT_TIME, cur_time_decimal(jiffies),
                           get_name_by_addr(__builtin_return_address(0)),
                           buf);
    }

    if (kmsg_raw_buff)
    {
        kbuffer_write(&kmsg_buffer, buf_procd, len);
    }

#ifndef KMSG_QUIET
    c_write_tty(buf_procd, len);
#endif // KMSG_QUIET

    return len;
}
