#include <kernel/mm.h>
#include <kernel/kmsg.h>
#include <errno.h>
#include <c/string.h>
#include <c/stdlib.h>
#include <fs/fcntl.h>

/* ldmap (load map) loads System.map file and automatically shows caller of printk
 *
 * ldmap offers a debug feature to let printk shows the proper function name
 * of the caller. Obviously this feature only works with DEBUG being defined
 */

#ifdef DEBUG

#define MIN(a, b) ((a) < (b) ? (a) : (b))

#define LDMAP_BUFF      1
#define LDMAP_BUFF_CLR  2

#define SYSMAP_BUFF_SIZE    4096
#define SYSMAP_BUFF_COUNT   8

#define SYSMAP_RECORD_LEN 1024

// System.map size should be below 32Kb
struct
__attribute__ ((packed))
sbuff_t
{
    char * data;
    size_t len;
};

struct
__attribute__ ((packed))
sysmap_t
{
    uint32_t addr;
    // char type;
    char name [32];
    struct sysmap_t * next;
    struct sysmap_t * last;
};

static struct sbuff_t * buff_pool = NULL;
static size_t buff_pool_off = 0;
static struct sysmap_t * sysmap = NULL;
static size_t sysmap_len = 0;
static size_t systrace_locked = 1;

#endif // DEBUG

/* out side DEBUG */
static char name_buff[12];

#ifdef DEBUG

int sys_open(const char *, int, int);
ssize_t sys_read(fd_t, char *, size_t, int);
int sys_close(int);

void load_buff(const char * __kernel buff, size_t size)
{
    size_t actual_size = MIN (size,
                              SYSMAP_BUFF_SIZE - buff_pool[buff_pool_off].len);

    if (actual_size == 0)
    {
        return;
    }

    memcpy(buff_pool[buff_pool_off].data + buff_pool[buff_pool_off].len,
           buff,
           actual_size);
    buff_pool[buff_pool_off].len += actual_size;

    if (buff_pool[buff_pool_off].len == SYSMAP_BUFF_SIZE)
    {
        if (buff_pool_off == SYSMAP_BUFF_COUNT)
        {
            return; // Buffer space full. abort...
        }

        buff_pool_off++;
    }

    return load_buff(buff + actual_size, size - actual_size);
}

void clear_buffer()
{
    buff_pool_off = 0;
    for (int i = 0; i < SYSMAP_BUFF_COUNT; i++)
    {
        buff_pool[i].len = 0;
    }
    sysmap_len = 0;
}

int init_buffer()
{
    buff_pool = malloc(sizeof (struct sbuff_t) * SYSMAP_BUFF_COUNT);

    if (!buff_pool)
    {
        goto failed;
    }

    for (int i = 0; i < SYSMAP_BUFF_COUNT; i++)
    {
        buff_pool[i].data = malloc(SYSMAP_BUFF_SIZE);
        buff_pool[i].len = 0;
    }

    sysmap = malloc(sizeof(struct sysmap_t));

    if (!sysmap)
    {
        goto failed;
    }

    struct sysmap_t * last = sysmap, * current;
    sysmap->last = NULL;

    for (ssize_t i = 0; i < SYSMAP_RECORD_LEN; i++)
    {
        current = malloc(sizeof(struct sysmap_t));

        if (!current)
        {
            goto failed;
        }

        current->next = NULL;
        current->last = last;

        last->next = current;

        last = current;
    }

    return 0;

failed:
    printk("malloc failed!");
    return -1;
}

#define TEMP_BUFF_LEN 4096

__attribute__((used))
int sys_ldmap(const char * __user filename, mode_t flag)
{
    char namebuff [128];
    char version [9];
    ssize_t offset;

    cstr_copy_from_user (namebuff, filename);

    switch (flag)
    {
        case LDMAP_BUFF:
        {
            char * buff = malloc(TEMP_BUFF_LEN);

            if (!buff)
            {
                printk("Insufficient memory!\n");
                return -ENOMEM;
            }

            if (!buff_pool || !sysmap)
            {
                if (init_buffer() != 0)
                {
                    printk("Failed to initialize buffer!\n");
                    return -ENOMEM;
                }
            }

            clear_buffer();

            int fd = sys_open(filename, O_RDONLY, 0777);
            if (fd <= 0)
            {
                printk("Open file %s failed! %d\n", namebuff, errno);
                return -errno;
            }

            // verify the System.map version
            offset = sys_read(fd, version, sizeof(version), READ_IN_KMODE);
            if (offset < 0 || memcmp(version, VERSION "\n", sizeof (version)) != 0)
            {
                sys_close(fd);
                printk("Unable to verify System.map version\n");
                return -EINVAL;
            }

            while ( (offset = sys_read(fd, buff, TEMP_BUFF_LEN, READ_IN_KMODE)) > 0)
            {
                if (offset < 0)
                {
                    clear_buffer();
                    printk("Read file %s failed! %d\n", namebuff, errno);
                    return -errno;
                }

                load_buff(buff, offset);
            }

            sys_close(fd);
            free(buff);

            if (format_raw_sysmap() == -1)
            {
                return -EINVAL;
            }

            break;
        }
        case LDMAP_BUFF_CLR:
            clear_buffer();
            break;
        default:
            printk("Unknown ldmap command `%d`\n", flag);
            return -EINVAL; // Invalid argument (POSIX.1-2001).
    }

    systrace_locked = 0;

    return 0;
}

#undef TEMP_BUFF_LEN

size_t read_pool(char * buff, size_t size, size_t off, void * starting)
{
    if (size == 0)
    {
        unsigned long ret = buff - (char*)starting;
        return ret;
    }

    size_t starting_pool = off / SYSMAP_BUFF_SIZE;
    size_t starting_addr = off - starting_pool * SYSMAP_BUFF_SIZE;
    size_t actual_rd_sz = MIN(size, buff_pool[starting_pool].len - starting_addr);

    if (starting_pool > SYSMAP_BUFF_COUNT)
    {
        return 0;
    }

    memcpy(buff,
           buff_pool[starting_pool].data + starting_addr,
           actual_rd_sz
           );

    return read_pool(buff + actual_rd_sz,
                     size - actual_rd_sz,
                     off + actual_rd_sz,
                     starting);
}

int format_raw_sysmap (void)
{
    char g_buff[9];
    g_buff [9] = 0;
    size_t read_off = 0;
    uint32_t tag_addr;
    char tag_type;
    struct sysmap_t * map = sysmap;

    while (1)
    {
        // read address
        if (read_pool(g_buff, 8, read_off, g_buff) != 8)
        {
            break;
        }
        read_off += 8;
        tag_addr = strtol(g_buff, NULL, 16);


        // read symbol type
        if (read_pool(g_buff, 3, read_off, g_buff) != 3)
        {
            break;
        }
        read_off += 3;
        tag_type = g_buff[1];


        // read symbol name
        int n_off = 0;
        char name[32] = { };
        while (n_off < 32)
        {
            if (read_pool(name + n_off, 1, read_off, name + n_off) != 1)
            {
                return -1;
            }
            read_off++;

            if (name[n_off] == '\n')
            {
                break;
            }

            n_off++;
        }

        name[n_off] = 0;


        // check if reached end of sysmap.map
        if (!memcmp(name, "end", 4)
            && tag_type == 'B')
        {
            break;
        }


        // only pick functions
        if (tag_type != 'T' && tag_type != 't')
        {
            continue;
        }


        map->addr = tag_addr;
        // map->type = tag_type;
        memcpy(map->name, name, n_off);


        if (sysmap_len != SYSMAP_RECORD_LEN && map->next)
        {
            map = map->next;
            sysmap_len++;
        }
        else
        {
            break;
        }
    }

    return 0;
}

#endif // DEBUG

const char * get_name_by_addr(uint32_t addr)
{
#ifdef DEBUG
    if (systrace_locked || !sysmap_len)
    {
        goto _skip;
    }

    struct sysmap_t * map = sysmap;

    for (size_t i = 0; i < sysmap_len; i++)
    {
        if (map->addr < addr && map->next->addr > addr)
        {
            return map->name;
        }

        map = map->next;
    }

_skip:
#endif // DEBUG
    __f_vsprintf(name_buff, "re:%08X", addr);
    return name_buff;
}
