/*
 *  kernel/panic.c
 */

/*
 * This function is used through-out the kernel (includeinh mm and fs)
 * to indicate a major problem.
 */
#define PANIC

#include <kernel/kernel.h>
#include <kernel/sched.h>
#include <kernel/kmsg.h>

int panic_mode = 0;

_Noreturn void panic(const char *s)
{
    panic_mode = 1;
    printk("[rt:%s]: %s",
           get_name_by_addr(__builtin_return_address(0)), s);

    if (current == task[0]) {
        printk("In swapper task - not syncing\n");
    } else {
        sys_sync();
    }

    __asm__("cli; hlt");
}
