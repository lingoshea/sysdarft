#define __LIBRARY__
#include <unistd.h>
#include <errno.h>
#include <asm/io.h>
#include <asm/system.h>
#include <kernel/kmsg.h>

#ifdef WITH_ACPI
#include <kernel/acpi.h>
#endif // WITH_ACPI

_syscall0(int, sync)

static void kb_wait(void)
{
    int i;

    for (i=0; i<0x10000; i++)
    {
        if ((inb_p(0x64) & 0x02) == 0) {
            break;
        }
    }
}

__attribute__((used))
long no_idt[2] = {0, 0};

// hard reset
_Noreturn void kbd_bios_reset()
{
    int i, j;
    extern unsigned long _pg0[1024];

    sti();

    // rebooting needs to touch the page at absolute addr 0
    _pg0[0] = 7;
    *((unsigned short *)0x472) = 0x1234;
    for (;;)
    {
        for (i = 0; i < 100; i++)
        {
            kb_wait();
            for(j = 0; j < 100000 ; j++);
            outb(0xfe,0x64);	 /* pulse reset low */
        }
        __asm__("lidt no_idt");
    }
}

#ifdef WITH_ACPI
_Noreturn
void order_shutdown(void)
{
    // order APM shutdown
    acpi_power_off();

    printk("ACPI shutdown apparently failed! Trying emulator-specific methods ...\n");

    // Emulator-specific methods:
    // In Bochs, and older versions of QEMU(than 2.0):

    outw(0x2000, 0xB004);

    // In newer versions of QEMU:
    outw(0x2000, 0x604);

    // In Virtualbox:
    outw(0x3400, 0x4004);

    // All methods failed above, halt the CPU
    panic("All known shutdown methods have failed!\n");
}
#endif // WITH_ACPI

__attribute__((used))
int sys_reboot(int command)
{
    switch (command)
    {
#ifdef WITH_ACPI
        case SYSRBT_SYSTEM_HALT:
            printk("!! SYSTEM SHUTDOWN IMMINENT !!\n");
            order_shutdown();
#endif // WITH_ACPI
        case SYSRBT_SYSTEM_REBOOT:
            printk("!! SYSTEM REBOOT IMMINENT !!\n");
            kbd_bios_reset();
#ifdef WITH_ACPI
        case SYSRBT_SYSTEM_SHUTDOWN:
            printk("!! SYSTEM SHUTDOWN IMMINENT !!\n");
            order_shutdown();
#endif // WITH_ACPI
        default:
            return -EINVAL; // Invalid argument (POSIX.1-2001).
    }
}
