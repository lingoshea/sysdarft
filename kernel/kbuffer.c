#include <kernel/kbuffer.h>
#include <asm/segment.h>
#include <errno.h>
#include <kernel/kernel.h>

// #define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

void kbuffer_init (char * buf, size_t len, kbuffer_t* buffer)
{
    buffer->buffer  = buf;
    buffer->size    = len;
    buffer->r_off   = 0;
    buffer->w_off   = 0;
    buffer->flags   = KBUFFER_CLEAR;
    buffer->err_count = 0;
}

char * kbuffer_advancing (kbuffer_t * buffer, char * cur)
{
    if (cur - buffer->buffer == buffer->size - 1)
    {
        return buffer->buffer;
    }

    return cur + 1;
}

size_t kbuffer_leng(kbuffer_t* buffer)
{
    size_t actual_read_len = buffer->w_off - buffer->r_off;

    if (actual_read_len < 0)
    {
        actual_read_len += buffer->size;
    } else if (actual_read_len == 0
        && buffer->flags & KBUFFER_FULL)
    {
        actual_read_len = buffer->size;
    }

    return actual_read_len;
}

ssize_t kbuffer_read (kbuffer_t* buffer, char * __user buf, size_t buf_len)
{
    size_t actual_read_len;
    char * cur;
    size_t i;

    actual_read_len = kbuffer_leng(buffer);
    actual_read_len = MIN(buf_len, actual_read_len);

    cur = buffer->buffer + buffer->r_off;
    for (i = 0; i < actual_read_len; i++)
    {
        if (buffer->flags & KBUFFER_2USER) {
            put_fs_byte(*cur, buf + i);
        } else { // kbuffer is in kernel mode
            buf[i] = *cur;
        }

        cur = kbuffer_advancing(buffer, cur);
    }

    return actual_read_len;
}

ssize_t kbuffer_write (kbuffer_t* buffer, const char * __kernel buf, size_t buf_len)
{
    char *cur;
    size_t i;
    size_t count_cd = 0;

    while (buffer->flags & KBUFFER_LOCK)
    {
        count_cd++;

        if (count_cd >= 0x0FFF  /* if buffer is unavailable for a long time */
                                /* this write operation will be cancelled */)
        {
            buffer->err_count++;
            return -EBUSY;
        }
    }

    // lock kbuffer
    buffer->flags |= KBUFFER_LOCK;

    cur = buffer->buffer + buffer->w_off;
    for (i = 0; i < buf_len; i++)
    {
        *cur = buf[i];
        cur = kbuffer_advancing(buffer, cur);
    }

    buffer->w_off += buf_len;

    // omit old info
    if (buffer->w_off > buffer->size
        && buffer->w_off > buffer->r_off)
    {
        while (buffer->w_off >= buffer->size) {
            buffer->w_off -= buffer->size;
        }

        buffer->r_off = buffer->w_off;
        buffer->flags |= KBUFFER_FULL;
    }

    // release kbuffer lock
    buffer->flags &= ~KBUFFER_LOCK;

    return buf_len;
}
