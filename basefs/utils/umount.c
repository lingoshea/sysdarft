#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

/* _syscall3(int, mount, char *, dev_name, char *, dir_name, int, rw_flag);
*/

int main(int argc, char ** argv)
{
    /* printf("umount %s\n", argv[1]); */ 
    if (umount (argv[1]) != 0)
    {
        printf("%s\n", strerror(errno));
        return 1;
    }
    
    return 0;
}

