#define __LIBRARY__
#define __NR_ldmap 75

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

_syscall2(int, ldmap, char *, buf, int, cmd);

char buff[4096] = { };

int main(int argc, char ** argv)
{
    int len, fd;
    if (argc != 2)
    {
        printf("%s [SYSTEM.MAP]\n", *argv);
        return 1;
    }

    if (ldmap(argv[1], 1) == 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

