#define __LIBRARY__
#define __NR_kmsg 73

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

_syscall3(int, kmsg, char *, buf, int, len, int, cmd);

char buff[4096] = { };

int main()
{
    int rt;
    rt = kmsg(buff, 4096, 0);
    printf("%s", buff);
    return rt;
}

