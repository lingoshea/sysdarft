#define __LIBRARY__
#define __NR_sysmap 11 

#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
_syscall1(int, unlink, const char *, fname);

int main(int argc, char ** argv)
{
    int ret = 0;
    int i;

    for (i = 1; i < argc; i++)
    {
        if (argv[i][0] == '-')
        {
            continue;
        }

        ret = unlink(argv[i]);
        if (ret != 0)
        {
            printf("Error: %s\n", strerror(errno));
            return ret;
        }
    }
    return 0;
}

