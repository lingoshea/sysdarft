#!/usr/bin/env python3
import os
import sys


# automatically set flags
def default_flags(marco, default=""):
    env_var = os.getenv(marco)
    if env_var == "":
        return default

    return env_var


# boot qemu in headless mode
def boot_qemu(kernel, init_root):

    return


# execute command in qemu
def exec_cmd(cmd):
    return


working_dir = default_flags("WORKING_DIR", "/tmp")
image_name = default_flags("IMG_NAME", "basefs.img")
