#define __LIBRARY__
#include <c/stdio.h>

#include <stdarg.h>
#include <unistd.h>

int __f_printf(char * printbuf, const char * fmt, va_list args)
{
    int len;
    len = vsprintf(printbuf, fmt, args);
    write(1, printbuf, len);

    return len;
}

char public_printf_buffer [1024];

int printf(const char * fmt, ...)
{
    va_list args;
    int ret;

    va_start(args, fmt);

    ret = __f_printf(public_printf_buffer, fmt, args);

    va_end(args);

    return ret;
}
