#ifdef WITH_ACPI
#include <asm/io.h>
#include <c/string.h>
#include <kernel/acpi.h>
#include <kernel/timer.h>
#include <sys/types.h>
#include <kernel/kmsg.h>
#include <kernel/mm.h>

#define MAJOR_NR 7
#include "blk.h"

const char * ACPI_SHUTDOWN_CMD = "poweroff\n";
#define ACPI_CMD_LEN 9

#define MIN(a, b) ((a) < (b) ? (a) : (b))

static struct {
    dword * smi_cmd;
    byte    flag_acpi_enable;
    byte    flag_acpi_disable;
    dword * pm1a_control;
    dword * pm1b_control;
    byte    pm1_control_len;
    word    slp_type_s5_a;
    word    slp_type_s5_b;
    word    slp_type_s1_a;
    word    slp_type_s1_b;
    word    slp_en;
    word    sci_en;
    word    if_inited;
} acpi_info = { .if_inited = 0 };

struct rsd_ptr_t {
    byte    signature[8];
    byte    checkSum;
    byte    oem_id[6];
    byte    revision;
    dword * rsd_addr;
};

struct facp_t {
    byte    signature[4];
    dword   length;
    byte    unneded1[40 - 8];
    dword * dsdt;
    byte    unneded2[48 - 44];
    dword * smi_cmd;
    byte    flg_acpi_enable;
    byte    flg_acpi_disable;
    byte    unneded3[64 - 54];
    dword * pm1a_ctrl_blk;
    dword * pm1b_ctrl_blk;
    byte    unneded4[89 - 72];
    byte    pm1_ctrl_len;
};

// check if the given address has a valid header
unsigned int *acpi_check_rsd_ptr(unsigned int *ptr)
{
    char *sig = "RSD PTR ";
    struct rsd_ptr_t *rsdp = (struct rsd_ptr_t *)ptr;
    byte *bptr;
    byte check = 0;
    int i;

    if (memcmp(sig, rsdp, 8) == 0)
    {
        // check checksum rsdpd
        bptr = (byte *)ptr;
        for (i = 0; i < sizeof(struct rsd_ptr_t); i++)
        {
            check += *bptr;
            bptr++;
        }

        // found valid rsdpd
        if (check == 0) {
            return (unsigned int *)rsdp->rsd_addr;
        }
    }

    return NULL;
}

// finds the acpi header and returns the address of the rsdt
unsigned int *acpi_get_rsd_ptr(void)
{
    unsigned int *addr;
    unsigned int *rsdp;

    // search below the 1mb mark for RSDP signature
    for (addr = (unsigned int *)0x000E0000; (int)addr < 0x00100000;
         addr += 0x10 / sizeof(addr))
    {
        rsdp = acpi_check_rsd_ptr(addr);

        if (rsdp != NULL) {
            return rsdp;
        }
    }

    // at address 0x40:0x0E is the RM segment of the ebda
    int ebda = *((short *)0x40E);    // get pointer
    ebda = ebda * 0x10 & 0x000FFFFF; // transform segment into linear address

    // search Extended BIOS Data Area for the Root System Description Pointer
    // signature
    for (addr = (unsigned int *)ebda; (int)addr < ebda + 1024;
         addr += 0x10 / sizeof(addr))
    {
        rsdp = acpi_check_rsd_ptr(addr);
        if (rsdp != NULL) {
            return rsdp;
        }
    }

    return NULL;
}

// checks for a given header and validates checksum
int acpi_check_header(unsigned int *ptr, char *sig)
{
    if (memcmp(ptr, sig, 4) == 0)
    {
        char *checkPtr = (char *)ptr;
        int len = (int)*(ptr + 1);
        char check = 0;
        while (0 < len--)
        {
            check += *checkPtr;
            checkPtr++;
        }

        if (check == 0)
        {
            return 0;
        }
    }

    return -1;
}

int acpi_enable(void)
{
    // check if acpi is enabled
    if ((inw((uint32_t)acpi_info.pm1a_control) & acpi_info.sci_en) == 0)
    {
        // check if acpi can be enabled
        if (acpi_info.smi_cmd != 0 && acpi_info.flag_acpi_enable != 0)
        {
            // send acpi enable command
            outb(acpi_info.flag_acpi_enable,
                 (unsigned int)acpi_info.smi_cmd);

            // give 3 seconds time to enable acpi
            int i;
            for (i = 0; i < 300; i++)
            {
                if ((inw((unsigned int)acpi_info.pm1a_control)
                & acpi_info.sci_en) == 1)
                {
                    break;
                }

                sleep(10);
            }

            if (acpi_info.pm1b_control != 0)
            {
                for (; i < 300; i++)
                {
                    if ((inw((unsigned int)acpi_info.pm1b_control)
                    & acpi_info.sci_en) == 1)
                    {
                        break;
                    }

                    sleep(10);
                }
            }

            if (i < 300)
            {
                printk("ACPI enabled\n");
                return 0;
            }
            else
            {
                printk("Couldn't enable ACPI\n");
                return -1;
            }
        }
        else
        {
            printk("No known way to enable ACPI\n");
            return -1;
        }
    }
    else
    {
        return 0;
    }
}

int find_Sx(int dsdtLength,
             char *SxAddr,
             struct facp_t *facp,
             word * char_a,
             word * char_b,
             const char * ct) // "_S5_"
{
    while (0 < dsdtLength--)
    {
        if (memcmp(SxAddr, ct, strlen(ct)) == 0)
        {
            break;
        }

        SxAddr++;
    }

    // check if \_Sx was found
    if (dsdtLength > 0)
    {
        // check for valid AML structure
        if ((*(SxAddr - 1) == 0x08
             || (*(SxAddr - 2) == 0x08 && *(SxAddr - 1) == '\\'))
            && *(SxAddr + 4) == 0x12)
        {
            SxAddr += 5;
            SxAddr += ((*SxAddr & 0xC0) >> 6) + 2; // calculate PkgLength size

            if (*SxAddr == 0x0A) {
                SxAddr++; // skip byteprefix
            }

            *char_a = *(SxAddr) << 10;
            SxAddr++;

            if (*SxAddr == 0x0A) {
                SxAddr++; // skip byteprefix
            }
            *char_b = *(SxAddr) << 10;

            printk("\\%s parse successful\n", ct);
            return 0;
        }
        else
        {
            printk("\\%s parse failed\n", ct);
        }
    }
    else
    {
        printk("\\%s not present\n", ct);
    }

    return -1;
}

int init_acpi(void)
{
    blk_dev[MAJOR_NR].request_fn = DEVICE_REQUEST;

    unsigned int *ptr = acpi_get_rsd_ptr();

    // check if address is correct  ( if acpi is available on this pc )
    if (ptr != NULL && acpi_check_header(ptr, "RSDT") == 0)
    {
        // the RSDT contains an unknown number of pointers to acpi tables
        int entrys = (int)*(ptr + 1);
        entrys = (entrys - 36) / 4;
        ptr += 36 / 4; // skip header information

        while (0 < entrys--)
        {
            // check if the desired table is reached
            if (acpi_check_header((unsigned int *)*ptr, "FACP") == 0)
            {
                entrys = -2;
                struct facp_t *facp = (struct facp_t *)*ptr;
                if (acpi_check_header((unsigned int *)facp->dsdt, "DSDT") == 0)
                {
                    // search the \_Sx package in the DSDT
                    char *SxAddr = (char *)facp->dsdt + 36; // skip header
                    int dsdtLength = (int)*(facp->dsdt + 1) - 36;
                    int error = 0;

                    error += find_Sx(dsdtLength,
                            SxAddr,
                            facp,
                            &acpi_info.slp_type_s5_a,
                            &acpi_info.slp_type_s5_b,
                            "_S5_");

                    acpi_info.smi_cmd = facp->smi_cmd;

                    acpi_info.flag_acpi_enable = facp->flg_acpi_enable;
                    acpi_info.flag_acpi_disable = facp->flg_acpi_disable;

                    acpi_info.pm1a_control = facp->pm1a_ctrl_blk;
                    acpi_info.pm1b_control = facp->pm1b_ctrl_blk;

                    acpi_info.pm1_control_len = facp->pm1_ctrl_len;

                    acpi_info.slp_en = 1 << 13;
                    acpi_info.sci_en = 1;

                    if (!error)
                    {
                        return 0;
                    }
                }
                else
                {
                    printk("DSDT invalid\n");
                }
            }
            ptr++;
        }

        printk("No valid FACP present\n");
    }
    else
    {
        printk("ACPI not detected\n");
    }

    printk(K_WARNING "ACPI not fully enabled!\n");
}

void acpi_power_off(void)
{
    // SCI_EN is set to 1 if acpi shutdown is possible
    if (acpi_info.sci_en == 0)
    {
        printk("ACPI: Shutdown not possible\n");
        return;
    }

    acpi_enable();

    // send the shutdown command
    outw(acpi_info.slp_type_s5_a | acpi_info.slp_en, (unsigned int)acpi_info.pm1a_control);

    if (acpi_info.pm1b_control != 0) {
        outw(acpi_info.slp_type_s5_b | acpi_info.slp_en, (unsigned int)acpi_info.pm1b_control);
    }

    printk(K_WARNING "ACPI: shutdown failed\n");
}

void do_acpi_request(void)
{
    int len = ACPI_CMD_LEN;
    char buff [ACPI_CMD_LEN];
    const auto cmd_list[] = { ACPI_SHUTDOWN_CMD };

    INIT_REQUEST
    if (MINOR(CURRENT->dev) != 1)
    {
        goto failed;
    }

    if (CURRENT->cmd == WRITE)
    {
        verify_area(CURRENT->buffer, len);
        memcpy(buff, CURRENT->buffer, len);

        if (!memcmp(buff, ACPI_SHUTDOWN_CMD, len))
        {
            acpi_power_off();
        }
        else
        {
            printk("ACPI: Invalid command\n");
            goto failed;
        }
    }
    else if (CURRENT->cmd == READ)
    {
        // TODO: BUFFER CONTROL NEEDED
        if (CURRENT->sector == 0)
        {
            int off = 0;
            for (int i = 0; i < (sizeof(cmd_list) / sizeof(char*)); i++)
            {
                memcpy(CURRENT->buffer + off, cmd_list[i], len - 1); // remove \n
                memcpy(CURRENT->buffer + off + len - 1, " ", 1);
                off += len;
            }

            CURRENT->buffer[off - 1] = '\n';
            CURRENT->bh->b_sp_wr_cntl = off;
        }
        else
        {
            CURRENT->bh->b_sp_wr_cntl = 0;
        }
    }
    else
    {
        printk("ACPI: Unknown request\n");
        goto failed;
    }

success:
    end_request(1);
    goto repeat;

failed:
    end_request(0);
    goto repeat;
}

#endif // WITH_ACPI
