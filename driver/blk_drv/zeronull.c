/*
 *  driver/blk_drv/zeronull.c
 */

#include <c/string.h>
#include <kernel/sched.h>
#include <kernel/fs.h>
#include <kernel/kernel.h>

#define MAJOR_NR 1
#include "blk.h"

// /dev/zero and /dev/null
void do_rd_request(void)
{
    int len;

    INIT_REQUEST
    len = CURRENT->nr_sectors << 9;
    if (CURRENT->cmd == READ)
    {
        // only output zero
        memset(CURRENT->buffer, 0, len);
    }

    end_request(1);
    goto repeat;
}

void zn_dev_init()
{
    blk_dev[MAJOR_NR].request_fn = DEVICE_REQUEST;
}
