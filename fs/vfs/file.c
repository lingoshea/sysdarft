#include <fs/vfs.h>
#include <kernel/kernel.h>
#include <kernel/mm.h>
#include <errno.h>
#include <fs/fs.h>

int vfs_mount(char * __user, char * __user dir_name, int rw_flag)
{
    char _dir_name[128];
    struct m_inode *dir_i;

    cstr_copy_from_user(_dir_name, dir_name);

    printk("mount VFS on %s (rw=%d)\n", _dir_name, rw_flag);

    if (!(dir_i = namei(dir_name)))
    {
        printk("mount failed! %s not found\n", _dir_name);
        return -ENOENT; // No such file or directory
    }

    if (dir_i->i_count != 1 || dir_i->i_num == ROOT_INO)
    {
        iput(dir_i);
        printk("mount failed! Target %s is busy\n", _dir_name);
        return -EBUSY;
    }

    if (!S_ISDIR(dir_i->i_mode))
    {
        iput(dir_i);
        printk("mount failed! Target %s is not a dir\n", _dir_name);
        return -EPERM;
    }

    if (dir_i->i_mount)
    {
        iput(dir_i);
        printk("mount failed! Target %s is already mounted\n", _dir_name);
        return -EPERM;
    }

    dir_i->i_mount = 1;
    dir_i->i_dirt = 1;
    /* NOTE! we don't iput(dir_i) */
    /* we do that in umount */

    printk("VFS: mount on %s\n", _dir_name);

    return 0;
}

int vfs_umount(char * __user dir_name)
{
    char _dir_name[128];
    struct m_inode *inode;

    cstr_copy_from_user(_dir_name, dir_name);

    printk("umount %s\n", _dir_name);

    if (!(inode = namei(dir_name)))
    {
        printk("Target %s not found\n", _dir_name);
        return -ENOENT;
    }

    if (inode->i_mount)
    {
        iput(inode);
        printk("%s is not mounted on\n", _dir_name);
        return -EINVAL;
    }

    if (!S_ISDIR(inode->i_mode))
    {
        printk("Target %s is not a directory!\n", _dir_name);
        return -EINVAL;
    }

    iput(inode);

    printk("VFS: %s successfully unmounted\n", _dir_name);

    return 0;
}
