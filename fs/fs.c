#include <fs/fs.h>
#include <fs/mfs.h>
#include <fs/vfs.h>
#include <c/string.h>

#define IVK_MFS 0
#define IVK_VFS 1

struct fs_ops_ivk_list_t fs_ops_ivk_list[] = {
        {
            .fs_magic = SUPER_MAGIC,
            .fs_ops =
                (struct fs_ops_t)
                {
                    .sys_ustat = mfs_ustat,
                    .sys_utime = mfs_utime,
                    .sys_access = mfs_access,
                    .sys_chdir = mfs_chdir,
                    .sys_chroot = mfs_chroot,
                    .sys_chmod = mfs_chmod,
                    .sys_chown = mfs_chown,
                    .sys_open = mfs_open,
                    .sys_creat = mfs_creat,
                    .sys_close = mfs_close,
                    .sys_mknod = mfs_mknod,
                    .sys_mkdir = mfs_mkdir,
                    .sys_rmdir = mfs_rmdir,
                    .sys_unlink = mfs_unlink,
                    .sys_link = mfs_link,
                    .sys_ioctl = mfs_ioctl,
                    .sys_pipe = mfs_pipe,
                    .sys_lseek = mfs_lseek,
                    .sys_read = mfs_read,
                    .sys_write = mfs_write,
                    .sys_stat = mfs_stat,
                    .sys_fstat = mfs_fstat,
                    .sys_umount = mfs_umount,
                    .sys_mount = mfs_mount,
                    .sys_sync = mfs_sync,
                    .sys_dup2 = mfs_dup2,
                    .sys_dup = mfs_dup,
                    .sys_fcntl = mfs_fcntl,
                }
        },

        {
            .fs_magic = 0x00,
            .fs_ops = { }
        }
};

// sync return 0 if success, -1 if otherwise
int sys_sync(void)
{
    int offset = 0;
    int error = 0;

    while (fs_ops_ivk_list[offset].fs_magic != 0)
    {
        if (fs_ops_ivk_list[offset].fs_ops.sys_sync)
        {
            if (fs_ops_ivk_list[offset].fs_ops.sys_sync() != 0)
            {
                error = -1;
            }
        }

        offset++;
    }

    return error;
}

int sys_close(unsigned int fd)
{
    return mfs_close(fd);
}

int sys_dup2(fd_t oldfd, fd_t newfd)
{
    return mfs_dup2(oldfd, newfd);
}

int sys_dup(fd_t fildes)
{
    return mfs_dup(fildes);
}

int sys_fcntl(fd_t fd, unsigned int cmd, unsigned long arg)
{
    return mfs_fcntl(fd, cmd, arg);
}

int sys_ioctl(fd_t fd, unsigned int cmd, unsigned long arg)
{
    return mfs_ioctl(fd, cmd, arg);
}

int sys_mknod(const char *filename, int mode, dev_t dev)
{
    return mfs_mknod(filename, mode, dev);
}

int sys_mkdir(const char *pathname, int mode)
{
    return mfs_mkdir(pathname, mode);
}

int sys_rmdir(const char *name)
{
    return mfs_rmdir(name);
}

int sys_unlink(const char *name)
{
    return mfs_unlink(name);
}

int sys_link(const char *oldname, const char *newname)
{
    return mfs_link(oldname, newname);
}

int sys_ustat(int dev, struct ustat *ubuf)
{
    return mfs_ustat(dev, ubuf);
}

int sys_utime(char *filename, struct utimbuf *times)
{
    return mfs_utime(filename, times);
}

int sys_access(const char *filename, int mode)
{
    return mfs_access(filename, mode);
}

int sys_chdir(const char *filename)
{
    return mfs_chdir(filename);
}

int sys_chroot(const char *filename)
{
    return mfs_chroot(filename);
}

int sys_chmod(const char *filename, int mode)
{
    return mfs_chmod(filename, mode);
}

int sys_chown(const char *filename, int uid, int gid)
{
    return mfs_chown(filename, uid, gid);
}

int sys_open(const char *filename, int flag, int mode)
{
    return mfs_open(filename, flag, mode);
}

int sys_creat(const char *pathname, int mode)
{
    return mfs_creat(pathname, mode);
}

int sys_pipe(unsigned long *fildes)
{
    return mfs_pipe(fildes);
}

int sys_lseek(unsigned int fd, off_t offset, int origin)
{
    return mfs_lseek(fd, offset, origin);
}

ssize_t sys_read(fd_t fd, char * __user buf, size_t count, int flags)
{
    return mfs_read(fd, buf, count, flags);
}

ssize_t sys_write(fd_t fd, char *buf, size_t count)
{
    return mfs_write(fd, buf, count);
}

ssize_t sys_stat(char *filename, struct stat *statbuf)
{
    return mfs_stat(filename, statbuf);
}

ssize_t sys_fstat(fd_t fd, struct stat *statbuf)
{
    return mfs_fstat(fd, statbuf);
}

int sys_umount(char * __user dev_name)
{
    return mfs_umount(dev_name);
}

int sys_mount(char * __user dev_name, char * __user dir_name, int rw_flag)
{
    char _dev_name[128];
    cstr_copy_from_user(_dev_name, dev_name);
    if (!strcmp(_dev_name, VFS_ENTRY))
    {
        return vfs_mount(dev_name, dir_name, rw_flag);
    }

    return mfs_mount(dev_name, dir_name, rw_flag);
}
