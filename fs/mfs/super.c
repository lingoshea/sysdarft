/*
 *  mfs/super.c
 */

/*
 * super.c contains code to handle the super-block tables.
 */
#include <asm/system.h>
#include <kernel/sched.h>
#include <kernel/config.h>
#include <kernel/mm.h>
//#include <kernel/kernel.h>
#include <kernel/kmsg.h>

#include <errno.h>
#include <sys/stat.h>

void wait_for_keypress(void);

/* set_bit uses setb, as gas doesn't recognize setc */
#define set_bit(bitnr, addr)                                                   \
  ({                                                                           \
    register int __res;                                                        \
    __asm__("bt %2,%3;setb %%al"                                               \
            : "=a"(__res)                                                      \
            : "a"(0), "r"(bitnr), "m"(*(addr)));                               \
    __res;                                                                     \
  })

struct super_block super_block[NR_SUPER];
/* this is initialized in init/main.c */
int ROOT_DEV = 0;

static void lock_super(struct super_block *sb)
{
    cli();
    while (sb->s_lock) {
        sleep_on(&(sb->s_wait));
    }
    sb->s_lock = 1;
    sti();
}

static void free_super(struct super_block *sb)
{
    cli();
    sb->s_lock = 0;
    wake_up(&(sb->s_wait));
    sti();
}

static void wait_on_super(struct super_block *sb)
{
    cli();
    while (sb->s_lock) {
        sleep_on(&(sb->s_wait));
    }
    sti();
}

struct super_block *get_super(dev_t dev)
{
    struct super_block *s;

    if (!dev) {
        return NULL;
    }

    s = 0 + super_block;
    while (s < NR_SUPER + super_block)
    {
        if (s->s_dev == dev)
        {
            wait_on_super(s);
            if (s->s_dev == dev) {
                return s;
            }
            s = 0 + super_block;
        } else {
            s++;
        }
    }

    return NULL;
}

void put_super(dev_t dev)
{
    struct super_block *sb;
    /* struct m_inode * inode;*/
    ssize_t i;

    if (dev == ROOT_DEV)
    {
        printk("root diskette changed: prepare for armageddon\n");
        return;
    }

    if (!(sb = get_super(dev))) {
        return;
    }

    if (sb->s_imount)
    {
        printk("Mounted disk changed - tssk, tssk\n");
        return;
    }

    lock_super(sb);
    sb->s_dev = 0;

    for (i = 0; i < I_MAP_SLOTS; i++) {
        brelse(sb->s_imap[i]);
    }

    for (i = 0; i < Z_MAP_SLOTS; i++) {
        brelse(sb->s_zmap[i]);
    }

    free_super(sb);
}

static struct super_block *read_super(dev_t dev)
{
    struct super_block *s;
    struct buffer_head *bh;
    int i, block;

    if (!dev) {
        return NULL;
    }

    check_disk_change(dev);
    if ((s = get_super(dev))) {
        return s;
    }

    for (s = 0 + super_block;; s++)
    {
        if (s >= NR_SUPER + super_block) {
            return NULL;
        }

        if (!s->s_dev) {
            break;
        }
    }

    s->s_dev        = dev;
    s->s_isup       = NULL;
    s->s_imount     = NULL;
    s->s_time       = 0;
    s->s_rd_only    = 0;
    s->s_dirt       = 0;
    lock_super(s);

    if (!(bh = bread(dev, 1)))
    {
        s->s_dev = 0;
        free_super(s);
        printk("Device %d unreadable\n", dev);
        return NULL;
    }

    *((struct d_super_block *)s) = *((struct d_super_block *)bh->b_data);
    brelse(bh);

    if (s->s_magic != SUPER_MAGIC)
    {
        s->s_dev = 0;
        free_super(s);
        printk("Unknown filesystem\n");
        return NULL;
    }

    for (i = 0; i < I_MAP_SLOTS; i++) {
        s->s_imap[i] = NULL;
    }

    for (i = 0; i < Z_MAP_SLOTS; i++) {
        s->s_zmap[i] = NULL;
    }

    block = 2;
    for (i = 0; i < s->s_imap_blocks; i++)
    {
        if ((s->s_imap[i] = bread(dev, block))) {
            block++;
        } else {
            break;
        }
    }

    for (i = 0; i < s->s_zmap_blocks; i++)
    {
        if ((s->s_zmap[i] = bread(dev, block))) {
            block++;
        } else {
            break;
        }
    }

    if (block != 2 + s->s_imap_blocks + s->s_zmap_blocks)
    {
        for (i = 0; i < I_MAP_SLOTS; i++) {
            brelse(s->s_imap[i]);
        }

        for (i = 0; i < Z_MAP_SLOTS; i++) {
            brelse(s->s_zmap[i]);
        }

        s->s_dev = 0;
        free_super(s);
        return NULL;
    }

    s->s_imap[0]->b_data[0] |= 1;
    s->s_zmap[0]->b_data[0] |= 1;
    free_super(s);
    return s;
}

__attribute__((used))
int mfs_umount(char * __user dev_name)
{
    char _dev_name[128];
    struct m_inode *inode;
    struct super_block *sb;
    int dev;

    cstr_copy_from_user(_dev_name, dev_name);

    printk("umount %s\n", _dev_name);

    if (!(inode = namei(dev_name)))
    {
        printk("Target %s not found\n", _dev_name);
        return -ENOENT;
    }

    if (inode->i_mount)
    {
        iput(inode);
        printk("%s is not mounted on\n", _dev_name);
        return -EINVAL;
    }

    dev = inode->i_zone[0];
    if (S_ISDIR(inode->i_mode))
    {
        printk("Redirecting to block device %d\n", inode->i_dev);
        dev = inode->i_dev;
    }

    iput(inode);

    if (dev == ROOT_DEV)
    {
        printk("Unmount filesystem root is not permitted\n");
        return -EBUSY;
    }

    if (!(sb = get_super(dev)))
    {
        printk("Target %s not found\n", _dev_name);
        return -ENOENT;
    }

    if (!(sb->s_imount))
    {
        printk("Target %s is not mounted\n", _dev_name);
        return -EINVAL; // Invalid argument
    }

    if (!sb->s_imount->i_mount) {
        printk("Mounted inode has i_mount=0\n");
    }

    for (inode = inode_table + 0; inode < inode_table + NR_INODE; inode++)
    {
        if (inode->i_dev == dev && inode->i_count)
        {
            printk("Device %s busy\n", _dev_name);
            return -EBUSY;
        }
    }

    sb->s_imount->i_mount = 0;
    iput(sb->s_imount);
    sb->s_imount = NULL;
    iput(sb->s_isup);
    sb->s_isup = NULL;
    put_super(dev);
    sync_dev(dev);

    printk("MFS: %s successfully unmounted\n", _dev_name);

    return 0;
}

__attribute__((used))
int mfs_mount(char * __user dev_name, char * __user dir_name, int rw_flag)
{
    char _dev_name[128], _dir_name[128];
    struct m_inode *dev_i, *dir_i;
    struct super_block *sb;
    int dev;

    cstr_copy_from_user(_dev_name, dev_name);
    cstr_copy_from_user(_dir_name, dir_name);

    printk("mount %s on %s (rw=%d)\n", _dev_name, _dir_name, rw_flag);

    if (!(dev_i = namei(dev_name)))
    {
        printk("mount failed! %s not found\n", _dev_name);
        return -ENOENT; // No such file or directory
    }

    dev = dev_i->i_zone[0];
    if (!S_ISBLK(dev_i->i_mode))
    {
        iput(dev_i);
        printk("%s is not a block device\n", _dev_name);
        return -EPERM; // Operation not permitted (POSIX.1-2001).
    }

    iput(dev_i);

    if (!(dir_i = namei(dir_name)))
    {
        printk("mount failed! Target %s not found\n", _dir_name);
        return -ENOENT; // No such file or directory
    }

    if (dir_i->i_count != 1 || dir_i->i_num == ROOT_INO)
    {
        iput(dir_i);
        printk("mount failed! Target %s is busy\n", _dir_name);
        return -EBUSY;
    }

    if (!S_ISDIR(dir_i->i_mode))
    {
        iput(dir_i);
        printk("mount failed! Target %s is not a dir\n", _dir_name);
        return -EPERM;
    }

    if (!(sb = read_super(dev)))
    {
        iput(dir_i);
        printk("mount failed! Read superblock in %s failed\n", _dev_name);
        return -EBUSY;
    }

    if (sb->s_imount)
    {
        iput(dir_i);
        printk("mount failed! %s is already mounted\n", _dev_name);
        return -EBUSY;
    }

    if (dir_i->i_mount)
    {
        iput(dir_i);
        printk("mount failed! Target %s is already mounted\n", _dir_name);
        return -EPERM;
    }

    sb->s_imount = dir_i;
    dir_i->i_mount = 1;
    dir_i->i_dirt = 1;  /* NOTE! we don't iput(dir_i) */
                        /* we do that in umount */

    printk("MFS: %s mount on %s\n", _dev_name, _dir_name);

    return 0;
}

void mount_root(void)
{
    int i, free;
    struct super_block *p;
    struct m_inode *mi;

    // check alignment
    VERIFY_BUILD_CONDITION(sizeof(struct d_inode) == 32);

    for (i = 0; i < NR_FILE; i++) {
        file_table[i].f_count = 0;
    }

    printk("System has reached mount root\n");
    printk("Device number for root is 0x%X\n", ROOT_DEV);

    if (MAJOR(ROOT_DEV) == 2)
    {
        __f_printk("Insert root filesystem floppy and press ENTER");
        wait_for_keypress();
    }

    printk("Mounting root filesystem\n");

    for (p = &super_block[0]; p < &super_block[NR_SUPER]; p++)
    {
        p->s_dev = 0;
        p->s_lock = 0;
        p->s_wait = NULL;
    }

    if (!(p = read_super(ROOT_DEV))) {
        panic("Unable to mount root");
    }

    if (!(mi = iget(ROOT_DEV, ROOT_INO))) {
        panic("Unable to read root i-node");
    }

    mi->i_count += 3; /* NOTE! it is logically used 4 times, not 1 */
    p->s_isup = p->s_imount = mi;
    current->pwd = mi;
    current->root = mi;
    free = 0;
    i = p->s_nzones;
    while (--i >= 0)
    {
        if (!set_bit(i & 8191, p->s_zmap[i >> 13]->b_data)) {
            free++;
        }
    }

    printk("DEVICE 0x%X: %d/%d free blocks\n", ROOT_DEV, free, p->s_nzones);

    free = 0;
    i = p->s_ninodes + 1;
    while (--i >= 0) {
        if (!set_bit(i & 8191, p->s_imap[i >> 13]->b_data)) {
            free++;
        }
    }

    printk("DEVICE 0x%X: %d/%d free inodes\n", ROOT_DEV, free, p->s_ninodes);
}
