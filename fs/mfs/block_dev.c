/*
 *  mfs/block_dev.c
 */

#include <errno.h>

#include <asm/segment.h>
#include <kernel/sched.h>

ssize_t block_write(dev_t dev, unsigned long * pos, char * buf, size_t count)
{
    dev_t block  = *pos >> BLOCK_SIZE_BITS;
    off_t offset = *pos & (BLOCK_SIZE - 1);
    ssize_t chars;
    ssize_t written = 0;
    struct buffer_head *bh;
    register char *p;

    while (count > 0)
    {
        chars = BLOCK_SIZE - offset;
        if (chars > count) {
            chars = count;
        }

        if (chars == BLOCK_SIZE) {
            bh = getblk(dev, block);
        }
        else {
            bh = breada(dev, block, block + 1, block + 2, -1);
        }

        block++;
        if (!bh) {
            return written ? written : -EIO;
        }

        p = offset + bh->b_data;
        offset = 0;
        *pos += chars;
        written += chars;
        count -= chars;
        while (chars-- > 0) {
            *(p++) = (char)get_fs_byte(buf++);
        }
        bh->b_dirt = 1;
        brelse(bh);
    }
    return written;
}

ssize_t block_read(dev_t dev, unsigned long * pos, char * buf, size_t count)
{
    block_t block  = *pos >> BLOCK_SIZE_BITS;
    off_t   offset = *pos & (BLOCK_SIZE - 1);
    ssize_t chars;
    ssize_t read = 0;
    struct buffer_head *bh;
    register char *p;

    while (count > 0)
    {
        chars = BLOCK_SIZE - offset;
        if (chars > count) {
            chars = count;
        }

        if (!(bh = breada(dev, block, block + 1, block + 2, -1)))
        {
            return read ? read : -EIO;
        }

        if (bh->b_sp_wr_cntl >= 0)
        {
            uint32_t _ret = bh->b_sp_wr_cntl;
            brelse(bh);
            return _ret;
        }

        block++;
        p = offset + bh->b_data;
        offset = 0;
        *pos  += chars;
        read  += chars;
        count -= chars;
        while (chars-- > 0) {
            put_fs_byte(*(p++), buf++);
        }

        brelse(bh);
    }
    return read;
}
