    .code16
# rewrite with AT&T syntax by falcon <wuzhangjin@gmail.com> at 081012
#
#    setup.s
#
# setup.s is responsible for getting the system data from the BIOS,
# and putting them into the appropriate places in system memory.
# both setup.s and system has been loaded by the bootblock.
#
# This code asks the bios for memory/disk/other parameters, and
# puts them in a "safe" place: 0x90000-0x901FF, ie where the
# boot-block used to be. It is then up to the protected mode
# system to read them from there before the area is overwritten
# for buffer-blocks.
#

# NOTE! These had better be the same as in bootsect.s!

    .equ INITSEG,  0x9000    # we move boot here - out of the way
    .equ SYSSEG,   0x1000    # system loaded at 0x10000 (65536).
    .equ SETUPSEG, 0x9020    # this is the current segment

.global _start, _bios_print

.text
.org 0

    ljmp    $SETUPSEG,  $_start


# unsigned char getchr()
getchr:
    xor     %ah,        %ah
    int     $0x16
    ret

###########################################################

_bios_print: # const char * (%bp), int (%cx)
    push    %ax
    push    %bx
    push    %cx
    push    %bp
    # 8 bytes

    # relocate stack pointer
    mov     %sp,            %bp
    add     $4*2+2,         %bp

    # query current cursor position
    mov     $0x03,          %ah
    xor     %bh,            %bh
    int     $0x10

    # output string
    mov     (%bp),          %cx
    mov     2(%bp),         %bp
    mov     $0x001F,        %bx
    mov     $0x1301,        %ax
    int     $0x10

    # restore registers
    pop     %bp
    pop     %cx
    pop     %bx
    pop     %ax
    ret

###########################################################

_start:
# setup segmentation
    mov     %cs,        %ax
    mov     %ax,        %ds
    mov     %ax,        %es

###########################################################

# notify user about entring setup
    push    $_setup_msg
    push    $_setup_msg_len
    call    _bios_print

    push    $_garz_info_msg
    push    $_garz_info_msg_len
    call    _bios_print

# reinit data segmentation
    mov     $INITSEG,   %ax     # this is done in bootsect already, but...
    mov     %ax,        %ds

###########################################################

# choose root device
    push    $_k_boot_dev_msg
    push    $_k_boot_dev_msg_len
    call    _bios_print
    call    getchr

    cmp     $'0',       %al
    je      _zero
    cmp     $'1',       %al
    je      _one
    cmp     $'2',       %al
    je      _two
    cmp     $'3',       %al
    je      _tri
    jmp     _end

_zero:
    movw    $0x21C,     %ds:508
    jmp     _end

_one:
    movw    $0x21D,     %ds:508
    jmp     _end

_two:
    movw    $0x301,     %ds:508
    jmp     _end

_tri:
    movw    $0x306,     %ds:508
    jmp     _end

_end:

###########################################################

# Get memory size (extended mem, kB)
    push    $_q_mem_sz_msg
    push    $_q_mem_sz_msg_len
    call    _bios_print

    clc
    mov     $0x88,      %ah
    int     $0x15
    mov     %ax,        %ds:2

    cmp     $7168,      %ax
    jae     _success_on_mem_dect

    push    $_insuff_mem
    push    $_insuff_mem_len
    call    _bios_print
    jmp     .

_success_on_mem_dect:

# Activate A20 NOW
###########################################################
    # if A20 is already activated, skip activation
    call    checka20
    cmp     $1,         %ax
    je      _a20_activated

    push    $_a20_starting
    push    $_a20_starting_len
    call    _bios_print

###########################################################
    # use BIOS Int $0x15
    push    $_a20_biosint15
    push    $_a20_biosint15_len
    call    _bios_print

    mov     $0x2401,    %ax
    int     $0x15

    # check A20 status
    call    checka20
    cmp     $1,         %ax
    je      _success
    push    $_a20_method_failed
    push    $_a20_method_failed_len
    call    _bios_print

###########################################################

    # use 8042 keyboard controller
    push    $_a20_kbd_ctrl
    push    $_a20_kbd_ctrl_len
    call    _bios_print

    call    empty_8042
    mov     $0xD1,      %al         # command write
    out     %al,        $0x64
    call    empty_8042
    mov     $0xDF,      %al         # A20 on
    out     %al,        $0x60
    call    empty_8042

    call    _long_dalay


    # check A20 status
    call    checka20
    cmp     $1,         %ax
    je      _success
    push    $_a20_method_failed
    push    $_a20_method_failed_len
    call    _bios_print

###########################################################

# Fast Gate A20 (there's a chance that this would not work)

    push    $_a20_fastgate
    push    $_a20_fastgate_len
    call    _bios_print

    in      $0x92,      %al
    test    $0x02,      %al
    jnz     after_a20fastgate
    or      $2,         %al
    and     $0xFE,      %al
    out     %al,        $0x92

after_a20fastgate:

    call    _long_dalay

    # if A20 is still failed, give up
    call    checka20
    cmp     $1,         %ax
    je      _success
    push    $_a20_method_failed
    push    $_a20_method_failed_len
    call    _bios_print
    jmp     _a20_activation_failed

###########################################################

_success:
    push    $_a20_method_success
    push    $_a20_method_success_len
    call    _bios_print

_a20_activated:
    push    $_a20_active
    push    $_a20_active_len
    call    _bios_print
    jmp     _after_a20

_a20_activation_failed:
    push    $_a20_inactive
    push    $_a20_inactive_len
    call    _bios_print

_after_a20:
    /*
    cli
    hlt
    jmp     .
    */

###########################################################

# Get video-card data:
    push    $_q_gpu_data_msg
    push    $_q_gpu_data_msg_len
    call    _bios_print

    mov     $0x0f,      %ah
    int     $0x10
    mov     %bx,        %ds:4   # bh = display page
    mov     %ax,        %ds:6   # al = video mode, ah = window width

###########################################################

# check for EGA/VGA and some config parameters
    mov     $0x12,      %ah
    mov     $0x10,      %bl
    int     $0x10
    mov     %ax,        %ds:8
    mov     %bx,        %ds:10
    mov     %cx,        %ds:12

###########################################################

# notify user about HDD data gathering
    push    $_q_hdd_data_msg
    push    $_q_hdd_data_msg_len
    call    _bios_print

###########################################################

# preserve current cursor position
    mov     $0x03,      %ah     # read cursor pos
    xor     %bh,        %bh
    int     $0x10               # save it in known place, con_init fetches

    # add one more line
    add     $1,         %dh
    mov     $0,         %dl

    mov     %dx,        %ds:0   # it from 0x90000

###########################################################

# Get hd0 data (16byte)
    mov     $0x0000,    %ax
    mov     %ax,        %ds
    lds     %ds:4*0x41, %si
    mov     $INITSEG,   %ax
    mov     %ax,        %es
    mov     $0x0080,    %di
    mov     $0x10,      %cx
    rep
    movsb

###########################################################

# Get hd1 data (16byte)
    mov     $0x0000,    %ax
    mov     %ax,        %ds
    lds     %ds:4*0x46, %si
    mov     $INITSEG,   %ax
    mov     %ax,        %es
    mov     $0x0090,    %di
    mov     $0x10,      %cx
    rep
    movsb

###########################################################

# modify ds
    mov     $INITSEG,   %ax
    mov     %ax,        %ds
    mov     $SETUPSEG,  %ax
    mov     %ax,        %es

###########################################################

# notify that we are ready to load system
    push    $_loading_system_msg
    push    $_loading_system_msg_len
    call    _bios_print

###########################################################

# Check that there IS a hd1 :-)
    mov     $0x01500,   %ax
    mov     $0x81,      %dl
    int     $0x13
    jc      no_disk1
    cmp     $3,         %ah
    je      is_disk1

no_disk1:
    mov     $INITSEG,   %ax
    mov     %ax,        %es
    mov     $0x0090,    %di
    mov     $0x10,      %cx
    mov     $0x00,      %ax
    rep
    stosb

is_disk1:
# now we want to move to protected mode ...
    cli            # no interrupts allowed !

# first we move the system to its rightful place

    mov     $0x0000,    %ax
    cld                         # 'direction'=0, movs moves forward
do_move:
    mov     %ax,        %es     # destination segment
    add     $0x1000,    %ax
    cmp     $0x9000,    %ax
    jz      end_move
    mov     %ax,        %ds     # source segment
    sub     %di,        %di
    sub     %si,        %si
    mov     $0x8000,    %cx
    rep
    movsw
    jmp     do_move

# then we load the segment descriptors

end_move:
    mov     $SETUPSEG,  %ax     # right, forgot this at first. didn't work :-)
    mov     %ax,        %ds

    lidt    idt_48              # load idt with 0,0
    lgdt    gdt_48              # load gdt with whatever appropriate


# well, that went ok, I hope. Now we have to reprogram the interrupts :-(
# we put them right after the intel-reserved hardware interrupts, at
# int 0x20-0x2F. There they won't mess up anything. Sadly IBM really
# messed this up with the original PC, and they haven't been able to
# rectify it afterwards. Thus the bios puts interrupts at 0x08-0x0f,
# which is used for the internal hardware interrupts as well. We just
# have to reprogram the 8259's, and it isn't fun.

    mov     $0x11,      %al     # initialization sequence(ICW1)
                                # ICW4 needed(1),CASCADE mode,Level-triggered
    out     %al,        $0x20   # send it to 8259A-1
    call    _delay
    out     %al,        $0xA0   # and to 8259A-2
    call    _delay
    mov     $0x20,      %al     # start of hardware int's (0x20)(ICW2)
    out     %al,        $0x21   # from 0x20-0x27
    call    _delay
    mov     $0x28,      %al     # start of hardware int's 2 (0x28)
    out     %al,        $0xA1   # from 0x28-0x2F
    call    _delay              #               IR 7654 3210
    mov     $0x04,      %al     # 8259-1 is master(0000 0100) --\
    out     %al,        $0x21   #                               |
    call    _delay              #                        INT    /
    mov     $0x02,      %al     # 8259-2 is slave(       010 --> 2)
    out     %al,        $0xA1
    call    _delay
    mov     $0x01,      %al     # 8086 mode for both
    out     %al,        $0x21
    call    _delay
    out     %al,        $0xA1
    call    _delay
    mov     $0xFF,      %al     # mask off all interrupts for now
    out     %al,        $0x21
    call    _delay
    out     %al,        $0xA1

# well, that certainly wasn't fun :-(. Hopefully it works, and we don't
# need no steenking BIOS anyway (except for the initial loading :-).
# The BIOS-routine wants lots of unnecessary data, and it's less
# "interesting" anyway. This is how REAL programmers do it.
#
# Well, now's the time to actually move into protected mode. To make
# things as simple as possible, we do no register set-up or anything,
# we let the gnu-compiled 32-bit programs do that. We just jump to
# absolute address 0x00000, in 32-bit protected mode.

    mov     %cr0,       %eax        # get machine status(cr0|MSW)
    bts     $0,         %eax        # turn on the PE-bit
    mov     %eax,       %cr0        # protection enabled

                                    # segment-descriptor        (INDEX:TI:RPL)
    .equ sel_cs0, 0x0008            # select for code segment 0 (  001:0 :00)
                                    #
    ljmp    $sel_cs0,   $0          # jmp offset 0 of code segment 0 in gdt

# This routine checks that the keyboard command queue is empty
# No timeout is used - if this hangs there is something wrong with
# the machine, and we probably couldn't proceed anyway.
empty_8042:
    call    _delay
    in      $0x64,      %al         # 8042 status port
    test    $2,         %al         # is input buffer full?
    jnz     empty_8042              # yes - loop
    ret

# void checka20()
checka20:
    pushf
    push    %ds
    push    %es
    push    %di
    push    %si

    cli

    xor     %ax,        %ax         # ax = 0
    mov     %ax,        %es

    not     %ax                     # ax = 0xFFFF
    mov     %ax,        %ds

    mov     $0x0500,    %di
    mov     $0x0510,    %si

    movb    %es:(%di),  %al
    push    %ax

    movb    %ds:(%si),  %al
    push    %ax

    movb    $0x00,      %es:(%di)
    movb    $0xFF,      %ds:(%si)

    cmpb    $0xFF,      %es:(%di)

    pop     %ax
    movb    %al,        %ds:(%si)

    pop     %ax
    mov     %al,        %es:(%di)

    mov     $0,         %ax

    je      __checka20_exit

    mov     $1,         %ax

__checka20_exit:
    pop     %si
    pop     %di
    pop     %es
    pop     %ds
    popf

    ret

#######################################

_long_dalay:
    push    %cx
    mov     $10,        %cx
_long_dalay_loop:
    cmp     $0,         %cx
    call    _delay
    dec     %cx
    jne     _long_dalay_loop

    pop     %cx
    ret

#######################################

_delay:
    .word 0x00eb, 0x00eb
    ret

_gdt:
    .word 0,0,0,0       # dummy

    .word 0x07FF        # 8Mb - limit=2047 (2048*4096=8Mb)
    .word 0x0000        # base address=0
    .word 0x9A00        # code read/exec
    .word 0x00C0        # granularity=4096, 386

    .word 0x07FF        # 8Mb - limit=2047 (2048*4096=8Mb)
    .word 0x0000        # base address=0
    .word 0x9200        # data read/write
    .word 0x00C0        # granularity=4096, 386

idt_48:
    .word 0             # idt limit=0
    .word 0,0           # idt base=0L

gdt_48:
    .word 0x800         # gdt limit=2048, 256 GDT entries
    .word 512+_gdt, 0x9 # gdt base = 0x9xxxx,
    # 512+gdt is the real gdt after setup is moved to 0x9020 * 0x10

_setup_msg:
    .ascii "Preparing preboot setup ..."
    .byte 13,10
    .equ _setup_msg_len, . - _setup_msg

_garz_info_msg:
    .ascii "Gathering infomation ... "
    .byte 13,10
    .equ _garz_info_msg_len, . - _garz_info_msg

_q_mem_sz_msg:
    .ascii "\n\rGet memory size ..."
    .byte 13,10
    .equ _q_mem_sz_msg_len, . - _q_mem_sz_msg

_q_gpu_data_msg:
    .ascii "Get video-card data ..."
    .byte 13,10
    .equ _q_gpu_data_msg_len, . - _q_gpu_data_msg

_q_hdd_data_msg:
    .ascii "Get HDD data ..."
    .byte 13,10
    .equ _q_hdd_data_msg_len, . - _q_hdd_data_msg

_no_disk1_msg:
    .ascii "Disk 1 not detected"
    .byte 13,10
    .equ _no_disk1_msg_len, . - _no_disk1_msg

_loading_system_msg:
    .ascii "Loading system ..."
    .byte 13,10
    .equ _loading_system_msg_len, . - _loading_system_msg

_dot:
    .ascii "."

_k_boot_dev_msg:
    .ascii "Choose a root device:\n\r"
    .ascii "[0]: floppy A\n\r"
    .ascii "[1]: floppy B\n\r"
    .ascii "[2]: 1st partition on hd0\n\r"
    .ascii "[3]: 1st partition on hd1\n\r"
    .ascii "Press ENTER to use the default device\n\r"
    .ascii "Your choice: "
    .equ _k_boot_dev_msg_len, . - _k_boot_dev_msg

_insuff_mem:
    .ascii "Insufficient memory, halting system ..."
    .byte 13, 10
    .equ _insuff_mem_len, . - _insuff_mem

_a20_active:
    .ascii "A20 line is active"
    .byte 13, 10
    .equ _a20_active_len, . - _a20_active

_a20_inactive:
    .ascii "Failed to activate A20, system boot failed"
    .byte 13, 10
    .equ _a20_inactive_len, . - _a20_inactive

_a20_starting:
    .ascii "A20 not activated, activating now ..."
    .byte 13, 10
    .equ _a20_starting_len, . - _a20_starting

_a20_biosint15:
    .ascii "Attempting to activate A20 using BIOSCall ..."
    .equ _a20_biosint15_len, . - _a20_biosint15

_a20_fastgate:
    .ascii "Attempting to activate A20 using FastGate ..."
    .equ _a20_fastgate_len, . - _a20_fastgate

_a20_kbd_ctrl:
    .ascii "Attempting to activate A20 using Kbd Ctrl ..."
    .equ _a20_kbd_ctrl_len, . - _a20_kbd_ctrl

_a20_method_success:
    .ascii " success."
    .byte 13, 10
    .equ _a20_method_success_len, . - _a20_method_success

_a20_method_failed:
    .ascii " failed."
    .byte 13, 10
    .equ _a20_method_failed_len, . - _a20_method_failed
